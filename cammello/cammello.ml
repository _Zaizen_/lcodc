open CamelGraphics

let _ =
  try
    canvas 1000 1000;
    home ()
    |> line_width 2
    |> label "A" |> forward 200 |> label "B" |> right |> forward 200
    |> label "C" |> right |> forward 200 |> label "D" |> right |> forward 200
    |> label " E" |> move_to 100 0  |> label "F" |> left  |> forward 300 |> label "G"
    |> color Color.blue
    |> repeat 4 ( fun pos -> pos |> left |> forward 120 |> label " H")
    |> color Color.green
    |> steps [ right; forward 150; label " I"; right; forward 150; label " L" ]
    |> home
    |> repeat_steps 3 [ forward 200; label "M"; left; forward 200; label "N"; ]
    |> home
    |> color Color.red
    |> left_by 60.0 |> forward 200 |> right_by 120.0 |> forward 200 |> right_by 20.0 |> move_by 20
    |> text "pippo" |> right_by 20.0 |> move_by 20 |> text "pertica" |> right_by 20.0 |> move_by 20
    |> text "e palla"
    |> stop;

    ignore(read_line ())
with _ -> canvas_close ()
