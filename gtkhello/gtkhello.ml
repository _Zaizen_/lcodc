let _ = GMain.init ()

let window = GWindow.window ~border_width: 10 ()

let message = "Hello Gtk"

let button = GButton.button ~label:message ~packing: window#add ()

let main () =
  window#event#connect#delete 
    ~callback:(fun _ -> prerr_endline "evento delete"; true);
  window#connect#destroy ~callback:GMain.quit;
  button#connect#clicked ~callback:(fun () -> prerr_endline message);
  button#connect#clicked ~callback:window#destroy;
  window#show ();
  GMain.main ()

let _ = Printexc.print main ()
