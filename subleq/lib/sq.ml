(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let hw_source =
  {|
  15 17 -1
  17 -1 -1
  16 1 -1
  16 3 -1
  15 15 0
  0 -1 72
  101 108 108
  111 44 32
  119 111 114
  108 100 33
  10 0
|}

let maxmem = 9999

type program = Raw of int array | Source of string

let source_to_istructions source =
  Str.split (Str.regexp "[ \t\n]+") source
  |> List.fold_left (fun acc e -> int_of_string e :: acc) []
  |> List.rev |> Array.of_list

let prepare_memory program =
  let mem =
    match program with
    | Raw program -> program
    | Source program -> source_to_istructions program
  in
  let ilen = Array.length mem in
  if ilen < maxmem then
    let padlen = maxmem - ilen in
    Array.append mem (Array.init padlen (fun _ -> 0))
  else failwith (Printf.sprintf "Max instructions set of %d" maxmem)

let uchar_to_int ?(pos = 0) text =
  String.get_utf_8_uchar text pos |> Uchar.utf_decode_uchar |> Uchar.to_int

let write_uchar_of_int i =
  let buf = Buffer.create 2 in
  Buffer.add_utf_8_uchar buf (Uchar.of_int i);
  let bytes = Buffer.to_bytes buf in
  Printf.printf "%s" (Bytes.to_string bytes)

let run program =
  let mem = prepare_memory program in
  let rec process ip mem =
    let ipA, ipB, ipC, ipN = (ip, ip + 1, ip + 2, ip + 3) in
    let ipNext =
      match (mem.(ipA), mem.(ipB), mem.(ipC)) with
      | -1, b, _c ->
          let line = read_line () in
          mem.(b) <- uchar_to_int line;
          ipN
      | a, -1, _c ->
          write_uchar_of_int mem.(a);
          ipN
      | a, b, c ->
          mem.(b) <- mem.(b) - mem.(a);
          if mem.(b) <= 0 then c else ipN
    in
    if ipNext < 0 then () else process ipNext mem
  in
  process 0 mem
