const std = @import("std");

fn fib(index: u64) u64 {
    const rfib = struct {
        fn call(n: u64, a: u64, b: u64) u64 {
            return switch (n) {
                0 => a,
                1 => b,
                else => call(n - 1, b, a + b),
            };
        }
    };
    return rfib.call(index, 0, 1);
}
pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    try stdout.print("{}\n", .{fib(45)});
}
