def fibTail (n : Nat) : Int := 
  fn n 0 1
  where 
    fn : Nat -> Nat -> Nat -> Int
      | 0, a, _ => a 
      | n + 1, a, b => fn n b (a + b)

def fibNaive (n : Nat) : Int :=
  match n with
  | 0 => 0
  | 1 => 1
  | (n + 2) => fibNaive n + fibNaive (n + 1)

unsafe def main (args : List String) : IO Unit :=
  let fib := 
    match args[0]? with 
    | some t => 
      match t with 
      | "naive" => Except.ok ( fun (n : Nat) => fibNaive n )
      | "tail" => Except.ok (fun (n : Nat) => fibTail n)
      | _ => Except.error "please specify naive or tail"
    | none => Except.error "please specify naive or tail"
  match fib with 
    | Except.ok fn => IO.println s!"{fn 45}"
    | Except.error m => IO.println s!"{m}"