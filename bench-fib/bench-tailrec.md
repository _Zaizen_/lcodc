| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `Zig/fib_opt` | 0.2 ± 0.1 | 0.2 | 0.7 | 1.00 |
| `Nim/fib_opt` | 0.7 ± 0.1 | 0.5 | 1.4 | 2.68 ± 0.69 |
| `Crystal/fib_opt` | 1.4 ± 0.2 | 1.1 | 3.0 | 5.68 ± 1.52 |
| `Chapel/fib_opt` | 13.5 ± 2.0 | 11.7 | 32.5 | 54.56 ± 14.63 |
| `Chapel/fib_iter` | 13.6 ± 1.9 | 11.7 | 29.0 | 54.82 ± 14.31 |
| `OCaml/fib_opt` | 1.3 ± 0.2 | 1.0 | 2.7 | 5.17 ± 1.35 |
| `OCaml/fib_memo` | 1.4 ± 0.2 | 1.1 | 3.0 | 5.58 ± 1.48 |
| `OCaml/fib_opt-unsafe` | 1.2 ± 0.2 | 0.9 | 2.6 | 4.81 ± 1.27 |
| `OCaml/fib_opt+flambda` | 1.2 ± 0.2 | 1.0 | 2.8 | 4.90 ± 1.32 |
| `OCaml/fib_opt+flambda-unsafe` | 1.2 ± 0.2 | 0.9 | 2.6 | 4.78 ± 1.27 |
| `OCaml/fib_binet` | 1.3 ± 0.2 | 1.0 | 2.8 | 5.20 ± 1.38 |
| `OCaml/fib_binet_opt` | 1.2 ± 0.2 | 1.0 | 2.7 | 4.83 ± 1.25 |
| `OCaml/fib_shift` | 1.3 ± 0.2 | 1.0 | 2.8 | 5.09 ± 1.32 |
| `OCaml/fib_binet+flambda` | 1.2 ± 0.2 | 1.0 | 2.7 | 4.90 ± 1.29 |
| `OCaml/fib_binet_opt+flambda` | 1.3 ± 0.2 | 1.0 | 2.7 | 5.14 ± 1.35 |
| `OCaml/fib_shift+flambda` | 1.2 ± 0.2 | 1.0 | 2.6 | 4.95 ± 1.29 |
| `MLton/fib_opt` | 0.9 ± 0.1 | 0.7 | 2.3 | 3.69 ± 0.99 |
| `MLton/fib_opt+native+O3` | 0.9 ± 0.1 | 0.7 | 2.3 | 3.77 ± 1.00 |
| `MLton/fib_opt-mlkit` | 1.0 ± 0.1 | 0.8 | 2.2 | 4.06 ± 1.04 |
| `MLton/fib_opt-mlkit-ngc` | 0.9 ± 0.1 | 0.7 | 2.2 | 3.83 ± 1.02 |
| `PolyML/fib_opt` | 3.3 ± 0.3 | 2.5 | 5.0 | 13.18 ± 3.17 |
| `Rust/fib_opt` | 0.8 ± 0.1 | 0.6 | 1.8 | 3.43 ± 0.95 |
| `Dylan/_build/bin/fib_opt` | 11.4 ± 1.4 | 8.5 | 16.2 | 45.91 ± 11.70 |
| `Go/fib_opt` | 1.0 ± 0.1 | 0.8 | 1.7 | 4.24 ± 1.05 |
| `ATS/fib_opt` | 0.6 ± 0.1 | 0.5 | 2.3 | 2.50 ± 0.68 |
| `Swift/fib_opt` | 1.1 ± 0.1 | 0.9 | 2.4 | 4.34 ± 1.14 |
| `Ada/fib_opt` | 0.7 ± 0.1 | 0.5 | 1.6 | 2.68 ± 0.72 |
| `Mercury/fib_opt` | 11.4 ± 1.1 | 9.1 | 15.4 | 46.11 ± 11.25 |
| `Mercury/fib_opt_memo` | 11.6 ± 1.3 | 9.3 | 15.6 | 46.92 ± 11.73 |
| `Mercury/fib_memo` | 11.2 ± 1.2 | 9.2 | 14.8 | 45.18 ± 11.22 |
| `C/fib_opt` | 0.6 ± 0.1 | 0.5 | 1.4 | 2.58 ± 0.70 |
| `C/fib_opt-clang` | 0.6 ± 0.1 | 0.5 | 1.5 | 2.52 ± 0.67 |
| `Kotlin/fib_opt` | 1.4 ± 0.2 | 1.1 | 2.9 | 5.78 ± 1.45 |
| `CLisp/fib_opt` | 8.3 ± 0.8 | 7.3 | 14.4 | 33.55 ± 8.14 |
| `Clean/bin/fib_opt` | 1.3 ± 0.2 | 1.0 | 2.8 | 5.19 ± 1.43 |
| `Koka/fib_opt` | 3.1 ± 0.5 | 2.4 | 5.5 | 12.35 ± 3.34 |
| `Terra/fib_opt` | 0.4 ± 0.1 | 0.3 | 0.9 | 1.79 ± 0.50 |
| `Oberon/fib-opt-voc` | 0.5 ± 0.1 | 0.3 | 1.3 | 2.04 ± 0.57 |
| `Oberon/fib-opt-obc` | 1.0 ± 0.1 | 0.8 | 2.3 | 4.03 ± 1.06 |
| `Oberon/fib-opt-obnc` | 1.1 ± 0.2 | 0.9 | 2.5 | 4.32 ± 1.15 |
| `Oberon/fib-iter-voc` | 0.5 ± 0.1 | 0.3 | 1.1 | 1.98 ± 0.55 |
| `Oberon/fib-iter-obc` | 1.0 ± 0.1 | 0.8 | 2.2 | 4.18 ± 1.10 |
| `Oberon/fib-iter-obnc` | 1.1 ± 0.2 | 0.8 | 2.3 | 4.37 ± 1.15 |
| `Pascal/fib_opt` | 0.3 ± 0.1 | 0.2 | 1.7 | 1.16 ± 0.38 |
| `Lean/build/bin/fib tail` | 3.3 ± 0.4 | 2.6 | 6.0 | 13.21 ± 3.39 |
| `ruby interpret/fib_opt.rb` | 79.4 ± 4.1 | 71.9 | 92.1 | 320.84 ± 73.33 |
| `ruby interpret/fib_opt_toc.rb` | 79.3 ± 3.7 | 71.8 | 84.7 | 320.67 ± 72.94 |
| `python interpret/fib_opt.py` | 30.4 ± 3.0 | 25.9 | 37.0 | 122.88 ± 29.92 |
| `node interpret/fib_opt.js` | 37.2 ± 2.6 | 33.9 | 45.3 | 150.52 ± 35.07 |
| `bun interpret/fib_opt.js` | 20.7 ± 2.0 | 16.4 | 26.6 | 83.71 ± 20.25 |
| `qjs interpret/fib_opt.js` | 4.2 ± 0.6 | 3.2 | 7.4 | 17.07 ± 4.51 |
| `js interpret/fib_opt.js` | 21.5 ± 1.3 | 19.8 | 26.7 | 86.84 ± 20.06 |
| `d8 interpret/fib_opt.js` | 8.5 ± 0.4 | 7.8 | 11.0 | 34.53 ± 7.84 |
| `tclsh interpret/fib_opt.tcl` | 8.2 ± 1.0 | 6.4 | 11.5 | 33.21 ± 8.43 |
| `interpret/fib_opt-qjsc` | 1.2 ± 0.2 | 0.9 | 2.7 | 4.98 ± 1.36 |
