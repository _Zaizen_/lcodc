:- module fib_opt.

:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.

:- implementation.
:- import_module int, list, string.

main(!IO) :-
    io.format("%d\n", [i(fib(45))], !IO).

:- func rfib(int, int, int) = int.
rfib(N, A, B) = R :-
    (
        N = 0, X = A;
        N = 1, X = B
    ) 
        -> R = X 
    ;  R = rfib(N - 1, B, A + B).

:- func fib(int) = int.
fib(N) = rfib(N, 0, 1).
