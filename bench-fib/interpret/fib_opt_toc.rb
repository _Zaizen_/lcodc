# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

RubyVM::InstructionSequence.compile_option = {
  tailcall_optimization: true,
  trace_instruction: false
}

(lambda {
  RubyVM::InstructionSequence.compile(<<-EOS).eval
    def fib(n)
      def fn(n, a, b)
        case n
        when 0
          a
        when 1
          b
        else
          fn(n - 1, b, a + b)
        end
      end
      fn(n, 0, 1)
    end
  EOS
}).call

puts send(:fib, 45)
