# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

proc fib {n} {
  proc fib:fn {n a b} {
    switch $n {
      0 { return $a }
      1 { return $b }
    }
    tailcall fib:fn [expr {$n - 1}] $b [expr {$a + $b}]
  }
  return [fib:fn $n 0 1]
}

puts [fib 45]