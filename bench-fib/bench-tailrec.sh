#!/bin/bash

BENCH="hyperfine  --warmup 3 -N"

echo "------------------------------"
echo "Fibonacci Tailrec"
echo "------------------------------"

$BENCH \
Zig/fib_opt \
Nim/fib_opt \
Crystal/fib_opt \
Chapel/fib_opt \
Chapel/fib_iter \
OCaml/fib_opt \
OCaml/fib_memo \
OCaml/fib_opt-unsafe \
OCaml/fib_opt+flambda \
OCaml/fib_opt+flambda-unsafe \
OCaml/fib_binet \
OCaml/fib_binet_opt \
OCaml/fib_shift \
OCaml/fib_binet+flambda \
OCaml/fib_binet_opt+flambda \
OCaml/fib_shift+flambda \
MLton/fib_opt \
MLton/fib_opt+native+O3 \
MLton/fib_opt-mlkit \
MLton/fib_opt-mlkit-ngc \
PolyML/fib_opt \
Rust/fib_opt \
Dylan/_build/bin/fib_opt \
Go/fib_opt \
ATS/fib_opt \
Swift/fib_opt \
Ada/fib_opt \
Mercury/fib_opt \
Mercury/fib_opt_memo \
Mercury/fib_memo \
C/fib_opt \
C/fib_opt-clang \
Kotlin/fib_opt \
CLisp/fib_opt \
Clean/bin/fib_opt \
Koka/fib_opt \
Terra/fib_opt \
Oberon/fib-opt-voc \
Oberon/fib-opt-obc \
Oberon/fib-opt-obnc \
Oberon/fib-iter-voc \
Oberon/fib-iter-obc \
Oberon/fib-iter-obnc \
Pascal/fib_opt \
"Lean/build/bin/fib tail" \
"ruby interpret/fib_opt.rb" \
"ruby interpret/fib_opt_toc.rb" \
"python interpret/fib_opt.py" \
"node interpret/fib_opt.js" \
"bun interpret/fib_opt.js" \
"qjs interpret/fib_opt.js" \
"js interpret/fib_opt.js" \
"d8 interpret/fib_opt.js" \
"tclsh interpret/fib_opt.tcl" \
interpret/fib_opt-qjsc \
Mojo/fib \
Mojo/fib_opt \
--export-markdown bench-tailrec.md \
--export-json bench-tailrec.json \
--export-csv bench-tailrec.csv
pandoc bench-tailrec.md -o bench-tailrec.html
pandoc bench-tailrec.md -o bench-tailrec.pdf

