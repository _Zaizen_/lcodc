local C = terralib.includec("stdio.h")

local terra rfib(n: int, a: int, b: int) : int
  switch n do
  case 0 then
    return a;
  case 1 then
    return b;
  else
    return rfib(n - 1, b, a + b);
  end
end

local terra fib(n: int) : int 
  return rfib(n, 0, 1)
end

local terra main()
    C.printf("%i\n", fib(45));
    return 0
end

return {
  main = main
}
