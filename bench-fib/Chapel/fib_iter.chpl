iter fib(n: int) {
  var (a, b) = (0, 1);
  for 0..n {
    yield a;
    (a, b) = (b, a + b);
  }
}
var s = fib(45);
writeln(s.last);
