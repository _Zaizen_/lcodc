#!/bin/bash

export CFLAGS="-O3 -mtune=native -march=native" 

#################################################
# Dylan and Swift with embedded llvm/clang executables
# Local installation and path precedence problems?
#################################################

cd Swift
echo "------------------------------"
echo "Swift"
echo "------------------------------"
activate-swift #local swift conflict with system clang?
swiftc -static-executable -Ounchecked -lto=llvm-thin fib.swift
strip ./fib
swiftc -static-executable -Ounchecked -lto=llvm-thin fib_opt.swift
strip ./fib_opt
deactivate-swift #local swift conflict with system clang?
cd ..

cd Dylan
echo "------------------------------"
echo "Dylan"
echo "------------------------------"
activate-dylan #local dylan conflict with system clang?
dylan-compiler -release -build fib.lid
dylan-compiler -release -build fib_opt.lid
strip ./_build/bin/*
strip ./_build/sbin/*
deactivate-dylan #local dylan conflict with system clang?
cd ..

#################################################

cd C
echo "------------------------------"
echo "COMPILE WITH CLANG"
echo "------------------------------"
CC="/usr/bin/clang -flto=thin "    

$CC fib.c -o fib-clang
strip fib-clang
$CC fib_opt.c -o fib_opt-clang
strip fib_opt-clang

echo "------------------------------"
echo "COMPILE WITH GCC"
echo "------------------------------"
CC="/usr/bin/gcc -flto "

$CC fib.c -o fib
strip fib
$CC fib_opt.c -o fib_opt
strip fib_opt
cd ..

cd Zig
echo "------------------------------"
echo "Zig"
echo "------------------------------"
zig build-exe fib.zig -O ReleaseFast
strip ./fib
rm ./zig-cache -Rf
zig build-exe fib_opt.zig -O ReleaseFast
strip ./fib_opt
rm ./zig-cache -Rf
cd ..

cd Nim
echo "------------------------------"
echo "Nim"
echo "------------------------------"
nim -d:release --mm:orc c fib.nim
nim -d:release --mm:orc c fib_opt.nim
strip ./fib
strip ./fib_opt
cd ..

cd Crystal
echo "------------------------------"
echo "Crystal"
echo "------------------------------"
crystal build fib.cr --release --no-debug
crystal build fib_opt.cr --release --no-debug
strip ./fib
strip ./fib_opt
cd ..

cd Chapel
echo "------------------------------"
echo "Chapel"
echo "------------------------------"
chpl --fast -o fib fib.chpl
strip ./fib
chpl --fast -o fib_opt fib_opt.chpl
strip ./fib_opt
chpl --fast -o fib_iter fib_iter.chpl
strip ./fib_iter
cd ..

OCAML_VERSION="5.0.0"

cd OCaml
echo "------------------------------"
echo "OCaml $OCAML_VERSION"
echo "------------------------------"
opam switch $OCAML_VERSION 
eval $(opam env --switch=$OCAML_VERSION)
ocamlopt fib.ml -o fib
ocamlopt -unsafe fib.ml -o fib-unsafe
ocamlopt fib_opt.ml -o fib_opt
ocamlopt -unsafe fib_opt.ml -o fib_opt-unsafe
ocamlopt fib_memo.ml -o fib_memo
ocamlopt fib_binet.ml -o fib_binet
ocamlopt fib_binet_opt.ml -o fib_binet_opt
ocamlopt fib_shift.ml -o fib_shift
strip ./fib
strip ./fib_opt
strip ./fib-unsafe
strip ./fib_opt-unsafe
strip ./fib_memo
strip ./fib_binet
strip ./fib_binet_opt
strip ./fib_shift
rm {*.o,*.cmx,*.cmi}

echo "------------------------------"
echo "OCaml $OCAML_VERSION+flambda"
echo "------------------------------"
opam switch $OCAML_VERSION+flambda 
eval $(opam env --switch=$OCAML_VERSION+flambda)
ocamlopt -O3 fib.ml -o fib+flambda
ocamlopt -O3 fib_opt.ml -o fib_opt+flambda
ocamlopt -O3 -unsafe fib.ml -o fib+flambda-unsafe 
ocamlopt -O3 -unsafe fib_opt.ml -o fib_opt+flambda-unsafe 
ocamlopt -O3 fib_memo.ml -o fib_memo+flambda
ocamlopt -O3 fib_binet.ml -o fib_binet+flambda
ocamlopt -O3 fib_binet_opt.ml -o fib_binet_opt+flambda
ocamlopt -O3 fib_shift.ml -o fib_shift+flambda

strip ./fib+flambda
strip ./fib_opt+flambda
strip ./fib+flambda-unsafe
strip ./fib_opt+flambda-unsafe
strip ./fib_memo+flambda
strip ./fib_binet+flambda
strip ./fib_binet_opt+flambda
strip ./fib_shift+flambda
rm {*.o,*.cmx,*.cmi}

opam switch $OCAML_VERSION 
eval $(opam env --switch=$OCAML_VERSION)
cd ..

cd MLton
echo "------------------------------"
echo "MLton"
echo "------------------------------"
mlton fib.sml
strip ./fib
mlton -codegen native -cc-opt -O3 -cc-opt -flto -output fib+native+O3 fib.sml
strip ./fib+native+O3 
mlton fib_opt.sml
strip ./fib_opt
mlton -codegen native -cc-opt -O3 -cc-opt -flto -output fib_opt+native+O3 fib_opt.sml
strip ./fib_opt+native+O3
cd ..

cd MLton
echo "------------------------------"
echo "MLKit"
echo "------------------------------"
mlkit -aopt -strip -o fib-mlkit fib.sml
mlkit -aopt -strip -o fib_opt-mlkit fib_opt.sml
mlkit -no_gc -aopt -strip -o fib-mlkit-ngc fib.sml
mlkit -no_gc -aopt -strip -o fib_opt-mlkit-ngc fib_opt.sml
cd ..

cd PolyML
echo "------------------------------"
echo "PolyML"
echo "------------------------------"
polyc fib.sml -o fib
strip ./fib
polyc fib_opt.sml -o fib_opt
strip ./fib_opt
cd ..

cd Rust
echo "------------------------------"
echo "Rust"
echo "------------------------------"
rustc -C opt-level=3 fib.rs -o fib
strip ./fib
rustc -C opt-level=3 fib_opt.rs -o fib_opt
strip ./fib_opt
cd ..

cd Go
echo "------------------------------"
echo "Go"
echo "------------------------------"
go build fib.go
strip ./fib
go build fib_opt.go
strip ./fib_opt
cd ..

cd ATS
echo "------------------------------"
echo "ATS"
echo "------------------------------"
patscc -O3 -mtune=native -march=native -flto -DATS_MEMALLOC_LIBC -latslib fib.dats -o fib
strip ./fib
patscc -O3 -mtune=native -march=native -flto -DATS_MEMALLOC_LIBC -latslib fib_opt.dats -o fib_opt
strip ./fib_opt
rm *.c
cd ..

cd Ada
echo "------------------------------"
echo "Ada"
echo "------------------------------"
gnatmake -f -O3 -mtune=native -march=native -flto -gnatp fib.adb
strip ./fib
gnatmake -f -O3 -mtune=native -march=native -flto -gnatp fib_opt.adb
strip ./fib_opt
cd ..

cd Mercury
echo "------------------------------"
echo "Mercury"
echo "------------------------------"
mmc -O4 --intermodule-optimization fib.m
strip ./fib
mmc -O4 --intermodule-optimization fib_memo.m
strip ./fib_memo
mmc -O4 --intermodule-optimization fib_opt.m
strip ./fib_opt
mmc -O4 --intermodule-optimization fib_opt_memo.m
strip ./fib_opt_memo
cd ..

cd Kotlin
echo "------------------------------"
echo "Kotlin native"
echo "------------------------------"
export PATH=$KOTLIN_NATIVE:$PATH
kotlinc-native fib.kt -e Fib.main -opt  -o fib
strip ./fib.kexe
mv fib.kexe fib
kotlinc-native fib_opt.kt -e FibOpt.main -opt  -o fib_opt
strip ./fib_opt.kexe
mv fib_opt.kexe fib_opt
cd ..

cd CLisp
echo "------------------------------"
echo "Common Lisp native (SBCL)"
echo "------------------------------"
sbcl --load build-fib.lisp
sbcl --load build-fib_opt.lisp
cd ..

cd Clean
echo "------------------------------"
echo "Clean"
echo "------------------------------"
clm -ngc -nst -nt -fusion -I src fib -o bin/fib
clm -ngc -nst -nt -fusion -I src fib_opt -o bin/fib_opt
cd ..

cd Koka
echo "------------------------------"
echo "Koka"
echo "------------------------------"
koka -O3 fib.kk -o fib
koka -O3 fib_opt.kk -o fib_opt
chmod 755 fib
chmod 755 fib_opt
cd ..

cd Terra
echo "------------------------------"
echo "Terra"
echo "------------------------------"
terra build.t
cd ..

cd Lean
echo "------------------------------"
echo "Lean"
echo "------------------------------"
lake build fib
cd ..

cd Oberon
echo "------------------------------"
echo "Oberon"
echo "------------------------------"
./build.sh
cd ..

cd Pascal
echo "------------------------------"
echo "Pascal"
echo "------------------------------"
./build.sh
cd ..


cd interpret
echo "------------------------------"
echo "QJC"
echo "------------------------------"
qjsc fib_opt.js -flto -o fib_opt-qjsc
chmod 755 fib_opt-qjsc
strip ./fib_opt-qjsc
cd ..

cd Mojo
echo "------------------------------"
echo "Mojo"
echo "------------------------------"
mojo build fib.mojo
mojo build fib_opt.mojo
strip ./fib
strip ./fib_opt
cd ..

echo "------------------------------"
echo "D8 mksnapshot"
echo "------------------------------"
mksnapshot --profile-deserialization --random-seed=314159265 --startup-blob=snapshot_blob.bin


