with Ada.Text_IO;

procedure Fib_opt is

  function Fib (n : Integer) return Integer is

    function RFib (n : Integer; a : Integer; b : Integer) return Integer is
    begin
      return
       (case n is when 0 => a, when 1 => b,
         when others     => RFib (n - 1, b, a + b));
    end RFib;

  begin
    return RFib (n, 0, 1);
  end Fib;

begin
  Ada.Text_IO.Put_Line (Integer'Image (Fib (45)));
end Fib_opt;
