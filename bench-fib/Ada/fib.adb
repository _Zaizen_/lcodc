with Ada.Text_IO;

procedure Fib is

   function Fib (n : Integer) return Integer is
   begin
      return (if n <= 2 then 1 else Fib (n - 1) + Fib (n - 2));
   end Fib;

begin
   Ada.Text_IO.Put_Line (Integer'Image (Fib (45)));
end Fib;
