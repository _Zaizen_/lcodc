#!/bin/bash

echo "------------------------------"
echo "Clean intermediate files"
echo "------------------------------"

find ./Ada -type f ! \( -name '*.adb' -or -perm -o=x \) -delete
find ./ATS -type f ! \( -name '*.dats' -or -perm -o=x \) -delete
find ./C -type f ! \( -name '*.c' -or -perm -o=x \) -delete
find ./Chapel -type f ! \( -name '*.chpl' -or -perm -o=x \) -delete
find ./Crystal -type f ! \( -name '*.cr' -or -perm -o=x \) -delete
find ./Go -type f ! \( -name '*.go' -or -perm -o=x \) -delete
find ./Mercury -type f ! \( -name '*.m' -or -perm -o=x \) -delete
find ./MLton -type f ! \( -name '*.sml' -or -perm -o=x \) -delete
find ./PolyML -type f ! \( -name '*.sml' -or -perm -o=x \) -delete
find ./Nim -type f ! \( -name '*.nim' -or -perm -o=x \) -delete
find ./OCaml -type f ! \( -name '*.ml' -or -perm -o=x \) -delete
find ./Rust -type f ! \( -name '*.rs' -or -perm -o=x \) -delete
find ./Swift -type f ! \( -name '*.swift' -or -perm -o=x \) -delete
find ./Zig -type f ! \( -name '*.zig' -or -perm -o=x \) -delete
find ./Kotlin -type f ! \( -name '*.kt' -or -perm -o=x \) -delete
find ./CLisp -type f ! \( -name '*.lisp' -or -perm -o=x \) -delete
find ./Terra -type f ! \( -name '*.t' -or -perm -o=x \) -delete
find ./Oberon -type f ! \( -name \*.obn -or -name \*.sh -or -perm -o=x \) -delete
find ./Oberon -type f ! \( -name \*.pas -or -name \*.sh -or -perm -o=x \) -delete

find ./Dylan -type f ! \( -name '*.dylan' -or -name '*.hdp' -or -name '*.lid' -or -perm -o=x \) -delete
rm ./Dylan/_build/build -Rf
rm ./Dylan/_build/databases -Rf
rm ./Dylan/_build/profiles -Rf

rm ./Clean/nitrile-lock.json
rm ./Clean/nitrile-packages -Rvf 
rm "./Clean/src/Clean System Files" -Rvf 

rm ./Lean/build/ir -Rvf 
rm ./Lean/build/lib -Rvf 

rm ./Koka/.koka -Rvf
