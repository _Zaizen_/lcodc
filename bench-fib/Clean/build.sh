# Copyright (c) 2023 Massimo Ghisalberti
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

clm -ngc -nst -nt -fusion -I src fib -o bin/fib
clm -ngc -nst -nt -fusion -I src fib_opt -o bin/fib_opt