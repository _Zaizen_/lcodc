module fib

import StdInt, StdEnv, StdDebug

fib::Int -> Int
fib n
	| n <= 2 = 1
	= fib (n - 1) + fib (n - 2)

Start::Int
Start = fib 45

/*
Start world
	# (console, world) = stdio world
	# console = fwrites ((toString (fib_opt 45)) +++ "\n") console
	# (ok, world) = fclose console world
	| not ok = abort "Cannot close console.\n"
	| otherwise = world
*/