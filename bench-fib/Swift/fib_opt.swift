func fib(n: Int) -> Int {
  func rfib(n: Int, a: Int, b: Int) -> Int {
    //n == 0 ? a : n == 1 ? b : rfib(n: n - 1, a: b, b: a + b)   
    switch n {
    case 0:
      return a
    case 1:
      return b
    default:
      return rfib(n: n - 1, a: b, b: a + b)
    } 
  }
  return rfib(n: n, a: 0, b: 1)
}
print(fib(n: 45))

