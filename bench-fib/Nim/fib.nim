proc fib (n: int): int =
  result = if n <= 2:
    1
  else:
    fib(n - 1) + fib(n - 2)

stdout.writeLine (fib(45))
