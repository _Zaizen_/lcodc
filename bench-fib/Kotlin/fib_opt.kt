package FibOpt

fun fib(n: Int): Int {
  fun rfib(n: Int, a: Int, b: Int): Int =
      when (n) {
        0 -> a
        1 -> b
        else -> rfib(n - 1, b, a + b)
      }
  return rfib(n, 0, 1)
}

fun main() {
  println(fib(45))
}
