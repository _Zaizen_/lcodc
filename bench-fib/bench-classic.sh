#!/bin/bash

BENCH="hyperfine --warmup 3"

echo "------------------------------"
echo "Fibonacci Classic"
echo "------------------------------"

$BENCH \
Zig/fib \
Nim/fib \
Crystal/fib \
Chapel/fib \
OCaml/fib \
OCaml/fib-unsafe \
OCaml/fib+flambda \
OCaml/fib+flambda-unsafe \
MLton/fib \
MLton/fib+native+O3 \
MLton/fib-mlkit \
MLton/fib-mlkit-ngc \
PolyML/fib \
Rust/fib \
Dylan/_build/bin/fib \
Go/fib \
ATS/fib \
Swift/fib \
Ada/fib \
Mercury/fib \
C/fib \
C/fib-clang \
Kotlin/fib \
CLisp/fib \
Clean/bin/fib \
Koka/fib \
Terra/fib \
Oberon/fib-voc \
Oberon/fib-obc \
Oberon/fib-obnc \
Pascal/fib \
"Lean/build/bin/fib naive" \
--export-markdown bench-classic.md \
--export-json bench-classic.json \
--export-csv bench-classic.csv
pandoc bench-classic.md -o bench-classic.html
pandoc bench-classic.md -o bench-classic.pdf
