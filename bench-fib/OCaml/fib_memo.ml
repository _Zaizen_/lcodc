let fib n =
  let mem = Hashtbl.create n in
  let vof fn n =
    try Hashtbl.find mem n with
    | Not_found ->
      let ret = fn n in
      Hashtbl.add mem n ret;
      ret
  in
  let rec mfib n =
    let ret =
      match n with
      | 0 -> 0
      | n when n < 0 -> failwith "negative argument"
      | n when n <= 2 -> 1
      | n -> vof mfib (n - 1) + vof mfib (n - 2)
    in
    ret
  in
  mfib n
;;

let _ = Printf.printf "%d\n" (fib 45)
