(*
   Binet formula

   phi = (1.0 +. sqrt5) /. 2.0)
*)

let fib n =
  let sqrt5 = sqrt 5.0 in
  1.0
  /. sqrt5
  *. (Float.pow ((1.0 +. sqrt5) /. 2.0) n -. Float.pow ((1.0 -. sqrt5) /. 2.0) n)
  |> int_of_float
;;

let _ = Printf.printf "%d\n" (fib 45.0)
