#!/bin/bash

echo "------------------------------"
echo "Clean All"
echo "------------------------------"

find ./Ada -type f ! -name '*.adb' -delete
find ./ATS -type f ! -name '*.dats' -delete
find ./C -type f ! -name '*.c' -delete
find ./Chapel -type f ! -name '*.chpl' -delete
find ./Crystal -type f ! -name '*.cr' -delete
find ./Go -type f ! -name '*.go' -delete
find ./Mercury -type f ! -name '*.m' -delete
find ./MLton -type f ! -name '*.sml' -delete
find ./PolyML -type f ! -name '*.sml' -delete
find ./Nim -type f ! -name '*.nim' -delete
find ./OCaml -type f ! -name '*.ml' -delete
find ./Rust -type f ! -name '*.rs' -delete
find ./Swift -type f ! -name '*.swift' -delete
find ./Zig -type f ! -name '*.zig' -delete
find ./Kotlin -type f ! -name '*.kt' -delete
find ./CLisp -type f ! -name '*.lisp' -delete
find ./Terra -type f ! -name '*.t' -delete
find ./Mojo -type f ! -name '*.mojo' -delete
find ./Oberon -type f ! \( -name \*.obn -or -name \*.sh \) -delete
find ./Pascal -type f ! \( -name \*.pas -or -name \*.sh \) -delete

#find ./Dylan -type f ! \( -name '*.dylan' -or -name '*.hdp' -or -name '*.lid' \) -delete
rm ./Dylan/_build -Rf

rm ./Clean/bin/*
rm ./Clean/nitrile-lock.json
rm ./Clean/nitrile-packages -Rvf 
rm "./Clean/src/Clean System Files" -Rvf 

find ./Koka -type f ! -name '*.kk' -delete
rm ./Koka/.koka -Rvf

rm ./Lean/build -Rvf

rm ./interpret/fib_opt-qjsc
rm ./snapshot_blob.bin
