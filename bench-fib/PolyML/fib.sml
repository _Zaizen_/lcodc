fun fib n =
    if n <= 2 then 1
    else fib (n - 1) + fib (n - 2)

fun main () =
    print ((Int.toString (fib 45)) ^ "\n")
    

