open Libmarkovc

let appfile = ref ""
let rulefile = ref ""
let infile = ref ""
let outfile = ref ""
let eval = ref ""
let verbose = ref false
let rules = ref ""
let source = ref ""

let usage =
  "markovc usage: \n"
  ^ Sys.argv.(0)
  ^ "\n"
  ^ "   --ruleset filename \n"
  ^ "   --infile filename \n"
  ^ "   --outfile filename \n"
  ^ "   --eval filename \n"
  ^ "   --verbose \n"
;;

let _args_parse =
  let opts =
    [ "--app", Arg.Set_string appfile, ": app file"
    ; "--ruleset", Arg.Set_string rulefile, ": ruleset file"
    ; "--infile", Arg.Set_string infile, ": in file"
    ; "--outfile", Arg.Set_string outfile, ": out file"
    ; "--eval", Arg.Set_string eval, ": eval source"
    ; "--verbose", Arg.Set verbose, ": verbose processing"
    ]
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let check_ruleset rulefile =
  if Sys.file_exists rulefile
  then rules := Compiler.open_source rulefile
  else (
    Printf.eprintf "ruleset %s not exists!\n" rulefile;
    exit 1)
;;

let eval_file infile =
  let eval_string eval =
    if eval <> String.empty
    then source := eval
    else (
      Printf.eprintf "please provide a source in --eval, --infile or --appfile\n";
      exit 1)
  in
  if infile <> String.empty && Sys.file_exists infile
  then source := Compiler.open_source infile
  else eval_string !eval
;;

let () =
  let lines =
    if !appfile <> String.empty
    then (
      eval_file !appfile;
      Compiler.compile_app ~verbose:!verbose !source |> String.concat "\n")
    else (
      check_ruleset !rulefile;
      eval_file !infile;
      Compiler.compile ~verbose:!verbose !rules !source |> String.concat "\n")
  in
  if !outfile <> String.empty
  then Helpers.write_file !outfile lines
  else print_endline lines
;;
