(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let fullstop = "."
let regexdelim = "/"

type tSource =
  | Source of string
  | SourceLines of string list

let is_terminator line = String.starts_with ~prefix:fullstop line

let is_regex line =
  String.starts_with ~prefix:regexdelim line && String.ends_with ~suffix:regexdelim line
;;

let strip_comments ?(remove_empty = false) source =
  let lines =
    match source with
    | Source source -> String.split_on_char '\n' source
    | SourceLines lines -> lines
  in
  let remempty memo s = if remove_empty && s = String.empty then memo else s :: memo in
  List.fold_left
    (fun memo line ->
      if String.starts_with ~prefix:"#" line
      then memo
      else remempty memo (String.trim line))
    []
    lines
  |> List.rev
;;

let process_ruleset source =
  let rules = strip_comments ~remove_empty:true (Source source) in
  let rp = Helpers.regexp {|(.*)\s+->\s+(.*)|} in
  let extract memo rule =
    let res = Re.exec_opt rp rule in
    match res with
    | None -> memo
    | Some m -> Helpers.get_groups2 m :: memo
  in
  List.fold_left extract [] rules |> List.rev
;;

let open_source = Helpers.open_file
let open_ruleset2 path = open_source path |> process_ruleset

let process string rule =
  let search, by = rule in
  let regex =
    if is_regex search
    then Helpers.regexp (Helpers.strip_first_last search)
    else Helpers.regexp (Helpers.escape_symbols search)
  in
  let s = Helpers.replace_all regex by string in
  if s = string then None else Some s
;;

let log ?(verbose = true) krule vrule line =
  if verbose then Printf.printf "%s\n%s -> %s\n%!" line krule vrule else ()
;;

let rewrite ?(verbose = false) ruleset line =
  let rec loop line rsets =
    match rsets with
    | (krule, vrule) :: rules ->
      log ~verbose krule vrule line;
      if is_terminator vrule
      then (
        match process line (krule, Helpers.strip_first vrule) with
        | None -> line
        | Some l -> l)
      else (
        match process line (krule, vrule) with
        | None -> loop line rules
        | Some l -> loop l ruleset)
    | [] -> line
  in
  loop line ruleset
;;

let compile ?(verbose = false) rules source =
  let ruleset = process_ruleset rules in
  let lines = strip_comments ~remove_empty:false (Source source) in
  List.map (rewrite ~verbose ruleset) lines
;;

let compile_app ?(verbose = false) source =
  let rp = Helpers.regexp "RULES:\n+(.*)\n+BEGIN:\n+(.*)" in
  let res = Re.exec_opt rp source in
  let rules, body =
    match res with
    | None -> failwith "Malformed app source"
    | Some m -> Helpers.get_groups2 m
  in
  let ruleset = process_ruleset rules in
  let lines =
    SourceLines (String.split_on_char '\n' body) |> strip_comments ~remove_empty:false
  in
  List.map (fun line -> rewrite ~verbose ruleset line) lines
;;
