(*******************************************************************************
   application: Convint - example of an integer converter in many bases
   module: conversions.ml
   version: 1
   year: 2017
   developer: Massimo Ghisalberti
   email: zairik@gmail.com
   licence: GPL3
 ******************************************************************************)

let list_of_int_range fn range =
  let min, max = range in
  List.init (max + 1 - min) (fun x -> fn (min + x))
;;

(* let ints_of_range range = list_of_int_range (fun n -> n) range *)

let chars_of_range range =
  let min, max = range in
  list_of_int_range char_of_int (int_of_char min, int_of_char max)
;;

let chars_of_ranges ranges =
  let op acc range = acc @ chars_of_range range in
  List.fold_left op [] ranges
;;

let symbols = chars_of_ranges [ '0', '9'; 'A', 'Z'; 'a', 'z' ]

let string_of_chars chars =
  let op c = String.make 1 c in
  String.concat "" (List.map op chars)
;;

(*
let string_of_chars chars =
  let buf = Buffer.create (List.length chars) in
  List.iter (Buffer.add_char buf) chars;
  Buffer.contents buf
;;
*)

let list_findi e list =
  let rec op i = function
    | [] -> raise Not_found
    | h :: t -> if h = e then i, e else op (i + 1) t
  in
  op 0 list
;;

let pow b e = int_of_float (float_of_int b ** float_of_int e)

let int_encode num base symbols =
  let rec op acc num =
    match num with
    | 0 -> acc
    | _ -> op (List.nth symbols (num mod base) :: acc) (num / base)
  in
  string_of_chars (op [] num)
;;

let int_decode alpha_num base symbols =
  let size = String.length alpha_num - 1 in
  let rec op acc pos =
    if pos < 0
    then acc
    else (
      let n, _ = list_findi alpha_num.[pos] symbols in
      let exp = size - pos in
      let weight = n * pow base exp in
      op (acc + weight) (pos - 1))
  in
  op 0 size
;;
