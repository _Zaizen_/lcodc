(*******************************************************************************
   application: Convint - example of an integer converter in many bases
   module: convint.ml
   version: 1
   year: 2017
   developer: Massimo Ghisalberti
   email: zairik@gmail.com
   licence: GPL3
 ******************************************************************************)

type number =
  | I of int
  | S of string

let num = ref (I 0)
let base = ref 16
let encode = ref true

let get_number n =
  try
    let num = int_of_string n in
    true, I num
  with
  | _ -> false, S n
;;

let as_int n =
  match n with
  | I x -> x
  | S x -> int_of_string x
;;

let as_string n =
  match n with
  | S x -> x
  | I x -> string_of_int x
;;

let args_parse =
  let opts =
    [ "-d", Arg.Clear encode, ": Force decode number"
    ; "-b", Arg.Set_int base, ": Base conversion 2..62"
    ]
  in
  let usage =
    "Integer conversion\nusage: "
    ^ Sys.argv.(0)
    ^ " number"
    ^ " -b BASE (default 16)"
    ^ " [-d (force decode)]"
  in
  let anons x =
    let to_encode, number = get_number x in
    num := number;
    encode := to_encode
  in
  Arg.parse opts anons usage
;;

let () =
  let result =
    match !encode with
    | true -> Conversions.int_encode (as_int !num) !base Conversions.symbols
    | false ->
      string_of_int (Conversions.int_decode (as_string !num) !base Conversions.symbols)
  in
  print_endline result
;;
