module PersonData = struct
  type t = { name : string; lastname : string}
end

module type Person = sig
  (* val data : PersonData.t *)
  val get_fullname : unit -> string
end

module Pico : Person = struct
  let data = PersonData.{ name = "Pico"; lastname = "De Paperis" }
  let get_fullname () = Printf.sprintf "%s %s" data.name data.lastname
end

module Paperino : Person = struct
  let data = PersonData.{ name = "Donald"; lastname = "Duck" }
  let get_fullname () = Printf.sprintf "%s %s" data.name data.lastname
end

let () =
  let persons = [
    (module Pico : Person); 
    (module Paperino : Person) ] 
  in
  let print_fullname m =
    let module P = (val m : Person) in
    print_endline (P.get_fullname ())
  in
  List.iter print_fullname persons
;;
