let ( /+ ) a b = float_of_int a /. float_of_int b |> ceil |> int_of_float
let ( /- ) a b = float_of_int a /. float_of_int b |> floor |> int_of_float

type kind =
  | Crypt
  | Decrypt

module UStr = CCUtf8_string

let ustr = UStr.unsafe_of_string
let ulength = UStr.n_chars
let slength = String.length
let llength = List.length
let ints_of_uchar us = List.rev (UStr.fold (fun acc uc -> Uchar.to_int uc :: acc) [] us)

let uchars_of_int il =
  List.rev (List.fold_left (fun acc i -> Uchar.of_int i :: acc) [] il)
;;

let worm n tpl =
  let rec fn n acc = if n <= 0 then acc else fn (n - 1) (UStr.append acc tpl) in
  fn n tpl
;;

let vigenere kind text pass =
  let pass' = ustr pass in
  let text' = ustr text |> ints_of_uchar in
  let ltext' = llength text' in
  let lpass = slength pass in
  let worm' = worm (ltext' /+ lpass) pass' |> ints_of_uchar in
  let op =
    match kind with
    | Crypt -> ( + )
    | Decrypt -> ( - )
  in
  let rec fn i acc = function
    | [] -> acc
    | h :: t -> fn (i + 1) (op h (List.nth worm' i) :: acc) t
  in
  let list = List.rev (fn 0 [] text') in
  uchars_of_int list |> UStr.of_list |> UStr.to_string
;;

let vigenere_xor text pass =
  let pass' = ustr pass in
  let text' = ustr text |> ints_of_uchar in
  let ltext' = llength text' in
  let lpass = slength pass in
  let worm' = worm (ltext' /+ lpass) pass' |> ints_of_uchar |> CCList.take ltext' in
  List.(
    combine text' worm'
    |> fold_left
         (fun acc e ->
           let e1, e2 = e in
           (e1 lxor e2 |> Uchar.of_int) :: acc)
         []
    |> rev)
  |> UStr.of_list
  |> UStr.to_string
;;

let () =
  let text = "È perché ma chissà!!!" in
  let pass = "x@<pi1%èpaa><!" in
  Printf.printf "messaggio: %s\n" text;
  Printf.printf "password: %s\n" pass;
  print_endline "-----------------------------";
  let obscured = vigenere Crypt text pass in
  let cleared = vigenere Decrypt obscured pass in
  Printf.printf "messaggio cifato: %s\n" obscured;
  Printf.printf "messaggio decifato: %s\n" cleared;
  print_endline "-----------------------------";
  let obscured2 = vigenere_xor text pass in
  let cleared2 = vigenere_xor obscured2 pass in
  Printf.printf "messaggio cifato: %s\n" obscured2;
  Printf.printf "messaggio decifato: %s\n" cleared2
;;
