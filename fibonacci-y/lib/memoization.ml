(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let memoizey ?(size = 12) compute =
  let table = Hashtbl.create size in
  fun fn x ->
    (*Printf.printf "find?: %d\n" value;*)
    try
      let value = Hashtbl.find table x in
      (*Printf.printf "found - get: %d with value: %d\n" x value;*)
      value
    with
    | Not_found ->
      let value = compute fn x in
      (*Printf.printf "not found - store: %d with value: %d\n" x value;*)
      Hashtbl.add table x value;
      value
;;

let memoize1 ?(size = 12) fn value =
  let memo compute =
    let table = Hashtbl.create size in
    fun key ->
      try
        let value = Hashtbl.find table key in
        (*Printf.printf "found - get: %d with value: %d\n" key value;*)
        value
      with
      | Not_found ->
        let value = compute key in
        (*Printf.printf "not found - store: %d with value: %d\n" key value;*)
        Hashtbl.add table key value;
        value
  in
  let fmemo_ref = ref (fun _ -> assert false) in
  let fmemo = memo (fun value -> fn !fmemo_ref value) in
  fmemo_ref := fmemo;
  fmemo value
;;

let memoize2 ?(size = 12) fn value1 value2 =
  let memo compute =
    let table = Hashtbl.create size in
    fun key1 key2 ->
      try
        let value = Hashtbl.find table (key1, key2) in
        (*Printf.printf "found - get: %d with value: %d\n" key value;*)
        value
      with
      | Not_found ->
        let value = compute key1 key2 in
        (*Printf.printf "not found - store: %d with value: %d\n" key value;*)
        Hashtbl.add table (key1, key2) value;
        value
  in
  let fmemo_ref = ref (fun _ -> assert false) in
  let fmemo = memo (fun value1 value2 -> fn !fmemo_ref value1 value2) in
  fmemo_ref := fmemo;
  fmemo value1 value2
;;
