(* Look-and-say - Conway sequence printer *)
(* MMG - zairik@gmail.com - Public Domain *)

module List = struct
  include List

  let last ls = nth ls (length ls - 1)
end

let make_list v l = List.init l (fun _ -> v)

let print ls =
  let str = String.concat "" (List.map string_of_int ls) in
  Printf.printf "%s\n" str
;;

let rec look_and_say data =
  match data with
  | [], result -> List.rev result
  | xhead :: xtail, [] -> look_and_say (xtail, [ xhead; 1 ])
  | xhead :: xtail, yhead :: n :: ytail when xhead = yhead ->
    look_and_say (xtail, yhead :: (1 + n) :: ytail)
  | xhead :: xtail, ylist -> look_and_say (xtail, xhead :: 1 :: ylist)
;;

let gen_look_and_say n l =
  let ls = make_list n l in
  let f acc _ =
    let last = List.last acc in
    acc @ [ look_and_say (last, []) ]
  in
  List.fold_left f (make_list [ n ] 1) ls
;;

let limit = ref 0
let digit = ref 1

let args_parse =
  let opts =
    [ "-d", Arg.Set_int digit, ": Digit number 0..9"
    ; "-l", Arg.Set_int limit, ": Recursion limit"
    ]
  in
  let usage =
    "las sequence calculator\nusage: " ^ Sys.argv.(0) ^ " [-d 0..9]" ^ " [-l number]"
  in
  Arg.parse opts (fun x -> raise (Arg.Bad ("Bad argument : " ^ x))) usage
;;

let () =
  args_parse;
  let digit = !digit in
  let limit = !limit in
  let result = gen_look_and_say digit limit in
  List.iter print result
;;
