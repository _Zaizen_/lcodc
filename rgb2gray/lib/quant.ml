(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let distance source target =
  let sr, sg, sb, sa = source in
  let tr, tg, tb, ta = target in
  let dr = tr - sr in
  let dg = tg - sg in
  let db = tb - sb in
  let da = ta - sa in
  Float.pow (float_of_int dr) 2.
  +. Float.pow (float_of_int dg) 2.
  +. Float.pow (float_of_int db) 2.
  +. Float.pow (float_of_int da) 2.
  |> Float.ceil
  |> int_of_float
;;

let white = 255, 255, 255, 255
let black = 0, 0, 0, 0
let max_distance = distance black white
let min_colors = 2

let color_frequencies img =
  let table = Hashtbl.create ~random:true 256 in
  let w = img.Image.width in
  let h = img.Image.height in
  let add_to_table r g b a =
    match Hashtbl.find_opt table (r, g, b, a) with
    | Some freq -> Hashtbl.replace table (r, g, b, a) (freq + 1)
    | None -> Hashtbl.add table (r, g, b, a) 1
  in
  for col = 0 to w - 1 do
    for row = 0 to h - 1 do
      Image.read_rgba img col row add_to_table
    done
  done;
  Hashtbl.fold (fun key value memo -> (key, value) :: memo) table []
  |> List.sort (fun a b ->
       let _, afreq = a in
       let _, bfreq = b in
       if afreq = bfreq then 0 else if afreq < bfreq then 1 else -1)
;;

let dump_color_frequencies img fname =
  let oc = Out_channel.open_text fname in
  let csvh = Printf.sprintf "freq,r,g,b,a,hex\n" in
  Out_channel.output_string oc csvh;
  List.iter
    (fun color_freq ->
      let color, freq = color_freq in
      let r, g, b, a = color in
      let text =
        let hex = Printf.sprintf "%x%x%x%x" r g b a in
        Printf.sprintf "%d,%d,%d,%d,%d,#%s\n" freq r g b a hex
      in
      Out_channel.output_string oc text)
    (color_frequencies img);
  Out_channel.close oc
;;

let strip_frequencies_from_colors frequencies =
  List.map
    (fun color_freq ->
      let color, _freq = color_freq in
      color)
    frequencies
;;

(*let color_frequencies_lists ?colors frequencies =
  let frequencies' = strip_frequencies_from_colors frequencies in
  match colors with
  | Some ncolors ->
    let freqs_length = List.length frequencies' in
    (*if ncolors < min_colors || ncolors >= freqs_length*)
    if ncolors >= freqs_length
    then frequencies', []
    else (
      let result =
        List.fold_left
          (fun memo color ->
            let index, high, low = memo in
            if index <= ncolors
            then index + 1, color :: high, low
            else index + 1, high, color :: low)
          (0, [], [])
          frequencies'
      in
      let _, high, low = result in
      List.rev high, List.rev low)
  | None -> frequencies', []
;;*)

let color_frequencies_list ?colors frequencies =
  let freqs = strip_frequencies_from_colors frequencies in
  match colors with
  | Some ncolors when ncolors >= List.length freqs -> freqs
  | Some ncolors -> List.filteri (fun index _ -> index < ncolors) freqs
  | None -> freqs
;;

let color_nearest rgba colors =
  let nearest =
    List.fold_left
      (fun memo color ->
        let dist = distance rgba color in
        let prevd, _ = memo in
        if dist < prevd then dist, color else memo)
      (max_distance, white)
      colors
  in
  let _, color = nearest in
  color
;;

let colors_reduce ?colors img =
  let frequencies = color_frequencies img in
  let freqs = color_frequencies_list ?colors frequencies in
  let w = img.Image.width in
  let h = img.Image.height in
  for col = 0 to w - 1 do
    for row = 0 to h - 1 do
      Image.read_rgba img col row (fun r g b a ->
        let r, g, b, a = color_nearest (r, g, b, a) freqs in
        Image.write_rgba img col row r g b a)
    done
  done;
  img
;;

let dump_colors_to_file ?(suffix = "") ?(dump_colors = true) fname img =
  let () =
    let fname = Printf.sprintf "%s.colors%s.csv" fname suffix in
    if dump_colors then dump_color_frequencies img fname else ()
  in
  ()
;;
