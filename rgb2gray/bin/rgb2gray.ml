(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

open Rgb_to_gray

let fred = ref Colors.fred
let fgreen = ref Colors.fgreen
let fblue = ref Colors.fblue
let color = ref false
let outflat = ref false
let inflat = ref false
let no_alpha = ref false
let infile = ref ""
let outfile = ref ""
let colors = ref 0
let dump_colors = ref false
let flat_channel = ref "r"

let usage =
  "rgb2gray usage: \n"
  ^ Sys.argv.(0)
  ^ "\n"
  ^ "   --in filename \n"
  ^ "   --out filename \n"
  ^ "   --fred float \n"
  ^ "   --fgreen float \n"
  ^ "   --fblue float \n"
  ^ "   --colors int \n"
  ^ "   --flat-channel [r|g|b|a|y] \n"
  ^ "   --color \n"
  ^ "   --outflat \n"
  ^ "   --inflat \n"
  ^ "   --no-alpha \n"
;;

let _args_parse =
  let opts =
    [ "--in", Arg.Set_string infile, ": input png file"
    ; "--out", Arg.Set_string outfile, ": output png file"
    ; "--fred", Arg.Set_float fred, ": red factor"
    ; "--fgreen", Arg.Set_float fgreen, ": green factor"
    ; "--fblue", Arg.Set_float fblue, ": blue factor"
    ; "--colors", Arg.Set_int colors, ": reduce to colors"
    ; ( "--flat-channel"
      , Arg.Set_string flat_channel
      , ": color channel for flat [r|g|b|a|y] " )
    ; "--color", Arg.Set color, ": preserve color"
    ; "--outflat", Arg.Set outflat, ": save flat image"
    ; "--inflat", Arg.Set inflat, ": input as flat image"
    ; "--no-alpha", Arg.Set no_alpha, ": no alpha in flat image"
    ; "--dump-colors", Arg.Set dump_colors, ": dump colors"
    ]
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let typeimage = if !color then Gray.RgbA else Gray.GreyA
let alpha_in_flat = if !no_alpha then Flat.NoAlpha else Flat.WithAlpha
let colors = if !colors >= Quant.min_colors then Some !colors else None

let channel =
  let c = String.get !flat_channel 0 in
  match c with
  | 'r' -> Flat.Red
  | 'g' -> Flat.Green
  | 'b' -> Flat.Blue
  | 'a' -> Flat.Alpha
  | 'y' -> Flat.Gray
  | _ -> Flat.Green
;;

let () =
  if Sys.file_exists !infile
  then (
    let img =
      if !inflat
      then Flat.open_image ~typeimage:alpha_in_flat !infile
      else Gray.open_image !infile
    in
    if !outflat
    then
      Flat.save_image
        ~typeimage:alpha_in_flat
        ~dump_colors:!dump_colors
        ~fred:!fred
        ~fgreen:!fgreen
        ~fblue:!fblue
        ?colors
        ~channel
        !outfile
        img
    else
      Gray.save_image
        ?colors
        ~fred:!fred
        ~fgreen:!fgreen
        ~fblue:!fblue
        ~typeimage
        ~dump_colors:!dump_colors
        ~inflat:!inflat
        !outfile
        img)
  else Printf.eprintf "infile %s not exists!\n" !infile
;;
