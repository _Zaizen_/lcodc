let chars_of_string s =
  let open String in
  List.init (length s) (fun x -> get s x)
;;

let ints_of_int i =
  let s = string_of_int i in
  let open String in
  let string_of_char = Printf.sprintf "%c" in
  List.init (length s) (fun x -> get s x |> string_of_char |> int_of_string)
;;

let digroot n =
  let rec fn acc = function
    | [] -> acc
    | h :: t when acc >= 10 -> fn h (t @ ints_of_int acc)
    | h :: t -> fn (acc + h) t
  in
  fn 0 (ints_of_int n)
;;

let digroot2 n =
  let r = n mod 9 in
  if r = 0 then 9 else r
;;

let digroot3 n = 1 + ((n - 1) mod 9)




let () = print_endline "Hello, World!"
