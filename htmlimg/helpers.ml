let range n = List.init n (fun x -> x)
let time () = Int32.of_float (Unix.time ())

let blit_from_string src src_off dst dst_off len =
  let open Bigarray.Array1 in
  for i = 0 to len - 1 do
    set dst (dst_off + i) (String.get src (src_off + i))
  done
;;
