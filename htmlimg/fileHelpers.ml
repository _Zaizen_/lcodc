let open_image = ImageLib_unix.openfile
let save_image = ImageLib_unix.writefile

let save_text file str =
  let channel = open_out file in
  output_string channel str;
  close_out channel
;;

let read_text_ch ic =
  let rec read content ic =
    try
      let content' = input_line ic :: content in
      read content' ic
    with
    | End_of_file -> Some (content |> List.rev |> String.concat "\n")
    | _ -> None
  in
  read [] ic
;;

let read_file path =
  let in_c = open_in path in
  let content = read_text_ch in_c in
  match content with
  | Some s -> s
  | None -> ""
;;
