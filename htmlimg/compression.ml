let deflate_string ?(level = 9) str =
  let open Bigarray.Array1 in
  let i = create Bigarray.char Bigarray.c_layout De.io_buffer_size in
  let o = create Bigarray.char Bigarray.c_layout De.io_buffer_size in
  let w = De.Lz77.make_window ~bits:15 in
  let q = De.Queue.create 4096 in
  let r = Buffer.create 4096 in
  let p = ref 0 in
  let cfg = Gz.Higher.configuration Gz.Unix Helpers.time in
  let refill dst =
    let len = min (dim dst) (String.length str - !p) in
    Helpers.blit_from_string str !p dst 0 len;
    p := !p + len;
    len
  in
  let flush src len =
    for i = 0 to len - 1 do
      Buffer.add_char r (unsafe_get src i)
    done
  in
  Gz.Higher.compress ~w ~q ~level ~refill ~flush () cfg i o;
  Buffer.contents r
;;

let inflate_string str =
  let open Bigarray.Array1 in
  let i = create Bigarray.char Bigarray.c_layout De.io_buffer_size in
  let o = create Bigarray.char Bigarray.c_layout De.io_buffer_size in
  let b = Buffer.create 4096 in
  let p = ref 0 in
  let refill dst =
    let len = min (dim dst) (String.length str - !p) in
    Helpers.blit_from_string str !p dst 0 len;
    p := !p + len;
    len
  in
  let flush src len =
    for i = 0 to len - 1 do
      Buffer.add_char b (unsafe_get src i)
    done
  in
  match Gz.Higher.uncompress ~refill ~flush i o with
  | Ok m -> Ok (m, Buffer.contents b)
  | Error _ as err -> err
;;

let compress_string = ImagePNG.PNG_Zlib.compress_string
