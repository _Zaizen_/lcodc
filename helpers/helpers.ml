let read_text_ch ic =
  let rec read content ic =
    try
      let content' = input_line ic :: content in
      read content' ic
    with
    | End_of_file -> Some (content |> List.rev |> String.concat "\n")
    | _ -> None
  in
  read [] ic
;;

let read_file path =
  let in_c = open_in path in
  let content = read_text_ch in_c in
  match content with
  | Some s -> s
  | None -> ""
;;

let block_stream_of_ch ic =
    Seq.of_dispenser (fun _ ->
      try Some (input_line ic) with
      | End_of_file -> None)

    (*
    Stream è deprecato da 4.14 e rimosso in 5.0
    Stream.from (fun _ ->
        try Some (input_line ic) with
        | End_of_file -> None) 
    *)
;;

let process_file_content ~op path =
  let ic = open_in path in
  try
    Stream.iter (fun data -> op data) (block_stream_of_ch ic);
    close_in ic
  with
  | e ->
    close_in ic;
    raise e
;;
