
let files_of_dir dir =
  let open List_utils in
  let fofd d = Array.to_list (Sys.readdir d) |> List.rev_map (Filename.concat d) in
  let dir_or_file (dirs, files) f = if Sys.is_directory f then f :: dirs, files else dirs, f :: files in
  let rec scan acc = function
    | [] -> acc
    | dir :: tail ->
      let dirs, files =
        List.fold_left dir_or_file ([], []) (fofd dir)
      in
      scan (files @> acc) (dirs @> tail)
  in
  scan [] [ dir ]
;;

let stream_of_ch ic =
    Seq.of_dispenser (fun _ ->
      try Some (input_line ic) with
      | End_of_file -> None)

    (*
    Stream è deprecato da 4.14 e rimosso in 5.0
    Stream.from (fun _ ->
        try Some (input_line ic) with
        | End_of_file -> None) 
    *)
;;

let lines_of_file ~op path =
  let ic = open_in path in
  let result = ref [] in
  try
    Seq.iter (fun data -> result := op data :: !result) (stream_of_ch ic);
    (*Stream.iter (fun data -> result := op data :: !result) (stream_of_ch ic);*)
    close_in ic;
    List.rev !result
  with
  | _ ->
    close_in ic;
    List.rev !result
;;
