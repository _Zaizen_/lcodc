let take_when fn list =
  let rec take result = function
    | [] -> result
    | h :: t when fn h -> take (h :: result) t
    | _ :: t -> take result t
  in
  take [] list |> List.rev
;;

let take_when_index fn list =
  let rec take i result = function
    | [] -> result
    | h :: t when fn i h -> take (i + 1) (h :: result) t
    | _ :: t -> take (i + 1) result t
  in
  take 0 [] list |> List.rev
;;

let take_until fn list =
  let rec take result = function
    | [] -> result
    | h :: _ when fn h -> result
    | h :: t -> take (h :: result) t
  in
  take [] list |> List.rev
;;

let take_until_index fn list =
  let rec take i result = function
    | [] -> result
    | h :: _ when fn i h -> result
    | h :: t -> take (i + 1) (h :: result) t
  in
  take 0 [] list |> List.rev
;;

let take_until_inc fn list =
  let rec take result = function
    | [] -> result
    | h :: _ when fn h -> h :: result
    | h :: t -> take (h :: result) t
  in
  take [] list |> List.rev
;;

let take_until_inc_index fn list =
  let rec take i result = function
    | [] -> result
    | h :: _ when fn i h -> h :: result
    | h :: t -> take (i + 1) (h :: result) t
  in
  take 0 [] list |> List.rev
;;

let slice x y list =
  let rec take i result = function
    | [] -> result
    | _ :: _ when i > y -> result
    | h :: t when i >= x && i <= y -> take (i + 1) (h :: result) t
    | _ :: t -> take (i + 1) result t
  in
  take 0 [] list |> List.rev
;;

let slice2 x y list = take_when_index (fun i _ -> i >= x && i <= y) list

let slices x y list =
  let rec take (left, center, right) i words =
    let i' = i + 1 in
    match words with
    | [] -> left, center, right
    | h :: t when i > y -> take (left, center, h :: right) i' t
    | h :: t when i >= x && i <= y -> take (left, h :: center, right) i' t
    | h :: t -> take (h :: left, center, right) i' t
  in
  let rev tp =
    let left, center, right = tp in
    List.rev left, List.rev center, List.rev right
  in
  take ([], [], []) 0 list |> rev
;;

let conc l1 l2 =
  let l1' = List.rev l1 in
  let rec op acc l =
    match l with
    | [] -> acc
    | h :: t -> op (h :: acc) t
  in
  op l2 l1'
;;

let ( @> ) = conc

let last l = List.nth l (List.length l - 1)
