let assocs k dic =
  List.fold_left
    (fun acc e ->
      let k', v = e in
      if k' = k then v :: acc else acc)
    []
    dic
;;

let assocs_opt k dic =
  let values =
    List.fold_left
      (fun acc e ->
        let k', v = e in
        if k' = k then v :: acc else acc)
      []
      dic
  in
  match values with
  | [] -> None
  | _ :: _ as r -> Some r
;;
