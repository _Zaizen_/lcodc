
function fact(fun) {
  return function (n) {
    return n == 0 ? 1 : n * fun(n - 1);
  };
}

function recFn(fun) {
  return fact(function(n) {
      return fun(fun)(n);
  });
}

var recFact = recFn(recFn);

console.log(`Phase4: ${recFact(5)}`);
