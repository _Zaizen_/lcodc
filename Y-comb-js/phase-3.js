
function fact(recFn) {
  return function (n) {
    return n == 0 ? 1 : n * recFn(n - 1);
  };
}

function recFn(n) {
  return fact(recFn)(n);
}

const recFact = fact(recFn);

console.log(`Phase3: ${recFact(5)}`);

