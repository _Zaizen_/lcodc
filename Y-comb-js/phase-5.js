
function fact(fun) {
  return function (n) {
    return n == 0 ? 1 : n * fun(n - 1);
  };
}

function recFn(fun) {
  return fact(function (n) {
    return fun(fun)(n);
  });
}

function Y() {
  return (function (fun) {
    return fun(fun);
  })(recFn);
}

const recFact = Y();

console.log(`Phase5: ${recFact(5)}`);
