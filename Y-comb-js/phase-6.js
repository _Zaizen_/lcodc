
function fact(fun) {
  return function (n) {
    return n == 0 ? 1 : n * fun(n - 1);
  };
}

function Y() {
  return (function (fun) {
    return fun(fun);
  })(function (fun) {
    return fact(function (n) {
      return fun(fun)(n)
    })
  }
  );
}

var recFact = Y();

console.log(`Phase6: ${recFact(5)}`);
