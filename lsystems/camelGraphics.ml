open Graphics

module Math = struct
  (** John Machin bapt. c. 1686 – June 9, 1751, Pi formula *)
  let pi = 4.0 *. ((4.0 *. atan (1.0 /. 5.0)) -. atan (1.0 /. 239.0))

  (* Leibniz formula for π *)
  let pi2 = 4.0 *. atan 1.0
  let degree_of_radian angle = angle *. 180.0 /. pi
  let radian_of_degree angle = angle *. pi /. 180.0
  let cos angle = cos (radian_of_degree angle)
  let sin angle = sin (radian_of_degree angle)
  let tX x = x + (size_x () / 2)
  let tY y = y + (size_y () / 2)
  let rX x angle = float_of_int x *. cos angle |> int_of_float
  let rY y angle = float_of_int y *. sin angle |> int_of_float
end

module Pos = struct
  type t =
    { h : float
    ; x : int
    ; y : int
    ; store : (float * int * int) list
    }
end

module Color = struct
  type t =
    | HexColor of string
    | IntColor of int

  let black = black
  let red = red
  let green = green
  let blue = blue
  let yellow = yellow
  let cyan = cyan
  let magenta = magenta
  let rgb r g b = rgb r g b
end

let line n pos =
  Pos.(
    let dx = Math.rX n pos.h + pos.x in
    let dy = Math.rY n pos.h + pos.y in
    lineto dx dy;
    { pos with x = dx; y = dy })
;;

let show_head pos =
  let opos = pos in
  ignore (line 10 pos);
  moveto opos.x opos.y;
  opos
;;

let forward = line
let head pos = Pos.{ pos with h = 0.0 }
let turn angle pos = Pos.{ pos with h = angle +. pos.h }
let flip angle pos = Pos.{ pos with h = 180.0 -. angle +. pos.h }
let right_by n = turn (-.n)
let left_by n = turn n
let right = right_by 90.0
let left = left_by 90.0

let home ?pos _ =
  let p =
    match pos with
    | None -> Pos.{ h = 90.0; x = 0; y = 0; store = [] }
    | Some t -> t
  in
  let x1 = Math.tX p.x in
  let y1 = Math.tY p.y in
  moveto x1 y1;
  Pos.{ h = p.h; x = x1; y = y1; store = [] }
;;

let storepush pos =
  Pos.(
    let store = (pos.h, pos.x, pos.y) :: pos.store in
    { pos with store })
;;

let storeclear pos = Pos.{ pos with store = [] }

let storepop pos =
  Pos.(
    match pos.store with
    | [] -> pos
    | hd :: tl ->
      let (h, x, y), store = hd, tl in
      { h; x; y; store })
;;

let move_to x y pos = home ~pos:Pos.{ pos with x; y } ()

let move_by n pos =
  Pos.(
    let dx = Math.rX n pos.h + pos.x in
    let dy = Math.rY n pos.h + pos.y in
    moveto dx dy;
    { pos with x = dx; y = dy })
;;

let position () = current_x (), current_y ()

let label s pos =
  let w, h = text_size " " in
  Pos.(moveto (pos.x + (w / 2)) (pos.y + (h / 3)));
  draw_string s;
  Pos.(moveto pos.x pos.y);
  pos
;;

let text s pos =
  let w, _ = text_size " " in
  Pos.(
    moveto (pos.x + w) pos.y;
    draw_string s;
    let x, y = position () in
    { pos with x; y })
;;

let color c pos =
  set_color c;
  pos
;;

let color2 c pos =
  let c' =
    match c with
    | Color.HexColor s ->
      let len = String.length s in
      if len > 0 then int_of_string ("0x" ^ String.sub s 1 (len - 1)) else 0
    | Color.IntColor i -> i
  in
  set_color c';
  pos
;;

let line_width n pos =
  set_line_width n;
  pos
;;

let repeat n fn pos =
  let rec loopf n fn pos =
    if n <= 0
    then pos
    else (
      let result = fn pos in
      loopf (n - 1) fn result)
  in
  loopf n fn pos
;;

let steps stps pos = stps |> List.fold_left (fun pos action -> pos |> action) pos

let repeat_steps_tr n stps pos =
  let rec loopf n stps pos =
    if n <= 0
    then pos
    else (
      let result = steps stps pos in
      loopf (n - 1) stps result)
  in
  loopf n stps pos
;;

let repeat_steps n stps pos =
  let cicles = List.init n (( + ) 1) in
  let actions pos _ = steps stps pos in
  List.fold_left actions pos cicles
;;

let stop ?pos _ =
  match pos with
  | _ -> ()
;;

let noop () pos = pos

let canvas w h =
  let ws = string_of_int w in
  let hs = string_of_int h in
  open_graph (Printf.sprintf " %sx%s" ws hs)
;;

let canvas_close = close_graph
