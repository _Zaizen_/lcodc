(**
 * Copyright (c) 2021 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let hd_opt = function
  | [] -> None
  | h :: _ -> Some h
;;

let hd_def d = function
  | [] -> d
  | h :: _ -> h
;;

let rec unfold f init =
  match f init with
  | None -> []
  | Some (x, next) -> x :: unfold f next
;;

let unfold_tl f init =
  let rec loop acc init =
    match f init with
    | None -> acc
    | Some (x, next) -> loop (x :: acc) next
  in
  List.rev (loop [] init)
;;

let zip l1 l2 =
  let gen (a, b) =
    match a, b with
    | [], [] | [], _ | _, [] -> None
    | ha :: tla, hb :: tlb -> Some ((ha, hb), (tla, tlb))
  in
  unfold gen (l1, l2)
;;

let zip_opt l1 l2 =
  let gen (a, b) =
    match a, b with
    | [], [] -> None
    | ha :: tla, hb :: tlb -> Some ((Some ha, Some hb), (tla, tlb))
    | [], hb :: tlb -> Some ((None, Some hb), (a, tlb))
    | ha :: tla, [] -> Some ((Some ha, None), (tla, b))
  in
  unfold gen (l1, l2)
;;

let range min max =
  let gen x = if x > max then None else Some (x, x + 1) in
  unfold gen min
;;

let zip2 list1 list2 =
  let rec loop acc l1 l2 =
    match l1, l2 with
    | [], [] | [], _ | _, [] -> acc
    | h1 :: rest1, h2 :: rest2 -> loop ((h1, h2) :: acc) rest1 rest2
  in
  List.rev (loop [] list1 list2)
;;

let zip_opt2 list1 list2 =
  let rec loop acc l1 l2 =
    match l1, l2 with
    | [], [] -> acc
    | [], h2 :: rest2 -> loop ((None, Some h2) :: acc) [] rest2
    | h1 :: rest1, [] -> loop ((Some h1, None) :: acc) rest1 []
    | h1 :: rest1, h2 :: rest2 -> loop ((Some h1, Some h2) :: acc) rest1 rest2
  in
  List.rev (loop [] list1 list2)
;;
