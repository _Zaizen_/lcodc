(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

type resolution =
  | Nano
  | Micro

let nseconds = 1_000_000_000.00

let time_m fn =
  let start = Unix.gettimeofday () in
  let pvalue = fn () in
  let stop = Unix.gettimeofday () in
  stop -. start, pvalue
;;

let time_n ?(seconds = true) fn =
  let start = Mtime_clock.now_ns () in
  let pvalue = fn () in
  let stop = Mtime_clock.now_ns () in
  let timing = Int64.sub stop start |> Int64.to_float in
  (if seconds then timing /. nseconds else timing), pvalue
;;

let time ?(resolution = Nano) ?(seconds = true) fn =
  match resolution with
  | Nano -> time_n ~seconds fn
  | Micro -> time_m fn
;;

let time_print_format ?(resolution = Nano) ?(seconds = true) ?(ln = false) () =
  let format =
    let n = if ln then "\n%!" else "" in
    match resolution with
    | Nano -> if seconds then "%.9fs" ^ n else "%fns" ^ n
    | Micro -> "%.6fs" ^ n
  in
  Scanf.format_from_string format "%f"
;;

let time_print ?(resolution = Nano) ?(seconds = true) fn =
  let t, _v = time ~resolution ~seconds fn in
  Printf.printf (time_print_format ~resolution ~seconds ~ln:false ()) t
;;

let time_string ?(resolution = Nano) ?(seconds = true) fn =
  let t, _v = time ~resolution ~seconds fn in
  Printf.sprintf (time_print_format ~resolution ~seconds ()) t
;;
