open Libtimize.Process

let program = ref ""
let args = ref ""
let verbose = ref false
let brief = ref false

let usage =
  "usage: "
  ^ Filename.basename Sys.argv.(0)
  ^ " [-p | --program programfile]"
  ^ " [-a | --args \"program args\"]"
  ^ " [--verbose]"
  ^ " [--brief]"
;;

let _args_parse =
  let opts =
    [ "-p", Arg.Set_string program, ": program file"
    ; "--program", Arg.Set_string program, ": program file"
    ; "-a", Arg.Set_string args, ": program args"
    ; "--args", Arg.Set_string args, ": program args"
    ; "--verbose", Arg.Set verbose, ": verbosity"
    ; "--brief", Arg.Set brief, ": brief"
    ]
  in
  let anons prog = program := prog in
  Arg.parse opts anons usage
;;

let verbosity pinfo =
  if !verbose
  then (
    Printf.printf "pid: %d\n" pinfo.pid;
    Printf.printf "exit_status: %s\n" pinfo.exit_status;
    Printf.printf "exit_code: %d\n" pinfo.exit_code;
    Printf.printf "output: %s\n" pinfo.output)
  else ()
;;

let nobriefity pinfo =
  if not !brief
  then (
    Printf.printf "program: %s\n" pinfo.program;
    if !args <> "" then Printf.printf "args: %s\n" pinfo.args else ())
  else ()
;;

let exetime pinfo =
  match pinfo.exetime with
  | TimeString s ->
    if !brief
    then Printf.printf "program: %s -> time: %s\n" pinfo.program s
    else Printf.printf "time: %s\n" s
  | _ -> ()
;;

let _ =
  if !program <> ""
  then (
    if !verbose then brief := false else ();
    if !brief then verbose := false else ();
    let pinfo = time_process !program !args in
    nobriefity pinfo;
    exetime pinfo;
    verbosity pinfo)
  else Printf.printf "%s\n" usage
;;
