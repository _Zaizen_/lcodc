open Parser
open Rules
open Html

let stext_filename = ref ""
let html_filename = ref "./out.html"

let args_parse =
  let opts =
    [ "-f", Arg.Set_string stext_filename, ": stext file"
    ; "-o", Arg.Set_string html_filename, ": HTML file"
    ]
  in
  let usage =
    "SText\nusage: " ^ Sys.argv.(0) ^ " [-f stext file]" ^ " [-o (HTML file)]"
  in
  let anons x = stext_filename := x in
  Arg.parse opts anons usage
;;

let () =
  match !stext_filename with
  | "" -> print_endline "Missing filename"
  | _ ->
    let stext = Helpers.read_file !stext_filename in
    let tree = parse (stext ^ "\n\n\n") (stext_rules (), def_rule ()) in
    (*[%pr tree];*)
    let h = html tree in
    Helpers.write_text_to_file h !html_filename
;;
