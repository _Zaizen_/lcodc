open Unix

type t =
  { year : int
  ; month : int
  ; day : int
  ; hour : int
  ; minute : int
  ; second : int
  }

let tm_of_time t =
  let _, i = modf t in
  Unix.localtime i
;;

let hour t =
  let { tm_hour; _ } = tm_of_time t in
  tm_hour
;;

let minute t =
  let { tm_min; _ } = tm_of_time t in
  tm_min
;;

let second t =
  let { tm_sec; _ } = tm_of_time t in
  tm_sec
;;

let day t =
  let { tm_mday; _ } = tm_of_time t in
  tm_mday
;;

let month t =
  let { tm_mon; _ } = tm_of_time t in
  tm_mon + 1
;;

let year t =
  let { tm_year; _ } = tm_of_time t in
  tm_year + 1900
;;

let now () =
  let tmt = time () in
  { year = year tmt
  ; month = month tmt
  ; day = day tmt
  ; hour = hour tmt
  ; minute = minute tmt
  ; second = second tmt
  }
;;

let string_of_dt ts =
  Printf.sprintf "%d-%02d-%02d %02d:%02d:%02d" 
  ts.year 
  ts.month 
  ts.day 
  ts.hour 
  ts.minute 
  ts.second
;;
