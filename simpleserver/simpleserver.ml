open Lwt
open Cohttp
open Cohttp_lwt_unix

let ansi_color_red = "\x1b[31m"
let ansi_color_green = "\x1b[32m"
let ansi_color_yellow = "\x1b[33m"
let ansi_color_blue = "\x1b[34m"
let ansi_color_magenta = "\x1b[35m"
let ansi_color_cyan = "\x1b[36m"
let ansi_color_reset = "\x1b[0m"

let log level msg =
  let date = Date_time.string_of_dt (Date_time.now ()) in
  let level', color =
    match level with
    | "d" -> "DEBUG", ansi_color_red
    | "w" -> "WARNING", ansi_color_yellow
    | "i" -> "INFO", ansi_color_green
    | _ -> level, ansi_color_cyan
  in
  Printf.printf "%s[%s][%s]%s %s\n%!" color date level' ansi_color_reset msg
;;

let log_request req =
  let uri = Request.uri req in
  let path = Uri.to_string uri in
  let meth = Request.meth req |> Code.string_of_method in
  log "ROUTE" (meth ^ ":" ^ path)
;;

let layout =
  format_of_string
    {html| 
  <html>
    <head>
      <meta charset="UTF-8" />
      <title>Page</title>
    </head>
    <body>
      %s
    </body>
  </html>
  |html}
;;

exception Not_a_file

let read_file fname =
  let open Lwt_io in
  Lwt.catch
    (fun _ ->
      Lwt_unix.stat fname
      >>= (fun stat ->
            (* log "d" "controllo se il file esiste ed è un file"; *)
            match stat.st_kind with
            | Unix.S_REG -> return fname
            | _ -> Lwt.fail Not_a_file)
      >>= fun fname ->
      (* log "d" "apertura del file"; *)
      open_file ~mode:input fname
      >>= fun ic ->
      (* log "d" "lettura file"; *)
      read ic
      >>= fun md ->
      (* log "d" "chiusura del file"; *)
      ignore (close ic);
      Lwt.return md)
    (function
      | exn -> Lwt.fail exn)
;;

let serve_json fname =
  let headers = Cohttp.Header.of_list [ "content-type", "application/json" ] in
  Lwt.catch
    (fun _ ->
      read_file fname
      >>= fun json -> Server.respond_string ~status:`OK ~headers ~body:json ())
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) | Not_a_file ->
        Server.respond_error ~status:`Not_found ~headers ~body:"Not_found" ()
      | exn -> Lwt.fail exn)
;;

let serve_md ?layout fname =
  let headers = Cohttp.Header.of_list [ "content-type", "text/html" ] in
  Lwt.catch
    (fun _ ->
      read_file fname
      >>= fun md ->
      let body = Omd.of_string md |> Omd.to_html in
      let tpl =
        match layout with
        | Some l -> format_of_string l
        | None -> format_of_string "%s"
      in
      let html = Printf.sprintf tpl body |> String.trim in
      Server.respond_string ~status:`OK ~headers ~body:html ())
    (function
      | Unix.Unix_error (Unix.ENOENT, _, _) | Not_a_file ->
        Server.respond_error ~status:`Not_found ~headers ~body:"Not_found" ()
      | exn -> Lwt.fail exn)
;;

let serve_file docroot uri =
  let fname = Server.resolve_file ~docroot ~uri in
  match Filename.extension fname with
  | ".md" -> serve_md fname ~layout
  | ".json" -> serve_json fname
  | _ -> Server.respond_file ~fname ()
;;

let serve docroot _conn req body =
  Cohttp_lwt.Body.to_string body
  >|= (fun body ->
        log_request req;
        body)
  (*>>= (fun body ->
        log_request req;
        Lwt.return body)*)
  >>= fun body ->
  match Request.meth req with
  | `GET ->
    let uri = Request.uri req in
    serve_file docroot uri
  | `POST -> Server.respond_string ~status:`OK ~body ()
  | _ -> Server.respond_error ~status:`Method_not_allowed ~body:"Method_not_allowed" ()
;;

let server docroot port =
  let server' = Server.make ~callback:(serve docroot) () in
  let mode = `TCP (`Port port) in
  Server.create ~mode server'
;;

let docroot = ref "./public"
let port = ref 8000

let args_parse =
  let opts =
    [ "-d", Arg.Set_string docroot, ": docroot"; "-p", Arg.Set_int port, ": server port" ]
  in
  let usage =
    "simpleserver\nusage: " ^ Sys.argv.(0) ^ " [-d docroot]" ^ " [-p server port]"
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let () =
  let date = Date_time.string_of_dt (Date_time.now ()) in
  Printf.printf
    "ReallySimpleServer started at: %s\nDocroot:%s\nPort:%d\nWaiting connection...\n%!"
    date
    !docroot
    !port;
  Lwt_main.run (server !docroot !port)
;;
