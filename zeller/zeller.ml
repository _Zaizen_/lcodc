let ( % ) a b =
  let m = a mod b in
  if m < 0 then m + b else m
;;

let day_names =
  [ "lunedì"; "martedì"; "mercoledì"; "giovedì"; "venerdì"; "sabato"; "domenica" ]
;;

let gregorian century year m d =
  (d + (13 * (m + 1) / 5) + year + (year / 4) + (century / 4) - (2 * century)) % 7
;;

let gregorian2 year m d =
  (d + (13 * (m + 1) / 5) + year + (year / 4) - (year / 100) + (year / 400)) % 7
;;

let year_month y m =
  match y, m with
  | y, 1 -> y - 1, 13
  | y, 2 -> y - 1, 14
  | _ -> y, m
;;

let to_iso_day d = ((d + 5) % 7) + 1

let zeller d m y =
  let year, month = year_month y m in
  let century = year / 100 in
  let year' = year % 100 in
  to_iso_day (gregorian century year' month d)
;;

let zeller2 d m y =
  let year, month = year_month y m in
  to_iso_day (gregorian2 year month d)
;;

let date = ref []

let args_parse =
  let opts = [] in
  let usage = "Zeller congruence\nusage: " ^ Sys.argv.(0) ^ " year" ^ " month" ^ " day" in
  let anons x = date := x :: !date in
  Arg.parse opts anons usage
;;

let unpack = function
  | [ d; m; y ] -> int_of_string d, int_of_string m, int_of_string y
  | _ -> failwith "Dati non validi"
;;

let two_digit n = if n < 10 then Printf.sprintf "0%d" n else Printf.sprintf "%d" n

let () =
  let year, month, day = unpack !date in
  let day_num = zeller day month year in
  let day_num2 = zeller day month year in
  let n = List.nth day_names (day_num - 1) in
  let n2 = List.nth day_names (day_num2 - 1) in
  Printf.printf
    "Data1 %s/%s/%s, %s\n"
    (two_digit day)
    (two_digit month)
    (two_digit year)
    n;
  Printf.printf
    "Data2 %s/%s/%s, %s\n"
    (two_digit day)
    (two_digit month)
    (two_digit year)
    n2
;;
