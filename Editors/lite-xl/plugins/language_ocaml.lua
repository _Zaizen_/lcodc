-- mod-version:3
local syntax = require "core.syntax"
local style = require "core.style"
local common = require "core.common"

local function patType(t, def)
  if style.syntax[t] ~= nil then return t else return def end
end

-- Additional styles

style.syntax["char"] = { common.color "#7cd4ff" }
style.syntax["type"] = { common.color "#cc91ff" }
style.syntax["type2"] = { common.color "#cc91ff" }
style.syntax["delimiter"] = { common.color "#ffa450" }
style.syntax["boolean"] = { common.color "#db5cdb" }
style.syntax["empties"] = { common.color "#ffd17e" }

--

syntax.add {
  files = {"%.ml$", "%.mli$" },
  comment = "(*",
  patterns = {
    { pattern = { "%(%*", "%*%)" },                   type = "comment" },
    { pattern = { "{[%l_]-|", "|[%l_]-}", '\\' },     type = "string"  },
    { pattern = "%[%]",                               type = patType("empties", "literal") },
    { pattern = "%[|",                                type = patType("delimiter", "literal") },
    { pattern = "|%]",                                type = patType("delimiter", "literal") },
    { pattern = "%[<",                                type = "operator" },    
    { pattern = "%[>",                                type = "operator" },    
    { pattern = "<%]",                                type = "operator" },    
    { pattern = ">%]",                                type = "operator" }, 
    { pattern = "%[",                                 type = patType("delimiter", "literal") },
    { pattern = "%]",                                 type = patType("delimiter", "literal") },
    { pattern = "{<",                                 type = "operator" },    
    { pattern = "{>",                                 type = "operator" },
    { pattern = "<}",                                 type = "operator" },    
    { pattern = ">}",                                 type = "operator" },
    { pattern = "%{",                                 type = patType("delimiter", "literal") },
    { pattern = "%}",                                 type = patType("delimiter", "literal") },
    { pattern = "%(%)",                               type = patType("empties", "literal") },
    { pattern = "%(",                                 type = patType("delimiter", "literal") },
    { pattern = "%)",                                 type = patType("delimiter", "literal") },    
    
    { pattern = "#+%a+",                               type = "function" }, 
    --{ pattern = "#",                                  type = "operator" }, 
    { pattern = "%.",                                 type = "operator" },
    { pattern = ":",                                  type = "operator" },
    { pattern = "[%+%-=/%*%^%%<>!~|&]",               type = "operator" }, 
    
    { pattern = "_",                                  type = "literal" },
    { pattern = "?",                                  type = "literal" },
    { pattern = "%(%)",                               type = "literal" },
    { pattern = "%[%]",                               type = "literal" },
    { pattern = "#[%a_][%w_]*",                       type = "function" },
    { pattern = { '"', '"', '\\' },                   type = "string" },    
    { pattern = { "{[%l_]-|", "|[%l_]-}", '\\' },     type = "string" },
    { pattern = "'\\?.'",                             type = patType("char", "string")  }, -- character literal
    { pattern = "0x[%da-fA-F]+",                      type = "number" },
    { pattern = "-?%d+[%d%.eE]*",                     type = "number" },
    { pattern = "-?%.?%d+",                           type = "number" },    
    { pattern = "[%u+][%w+%_+]+",                     type = patType("type2", "symbol") },
    { pattern = "[%+%-=/%*%^%%<>!~|&]",               type = "operator" },
    { pattern = "%f[^%.>]%l[%w_]*",                   type = "function" },
    { pattern = "%l[%w_]*%f[(]",                      type = "function" },
    { pattern = "%u[%w_]*",                           type = "keyword"  },     
    
    { pattern = "let%s+",                             type = "keyword" },   
    { pattern = "let%+%s+",                           type = "keyword" },
    { pattern = "let%+%w+",                           type = "normal" },
    { pattern = "let%*%s+",                           type = "keyword" },
    { pattern = "let%*%w+",                           type = "normal"},
    
    { pattern = "[%l_][%w_%.]*",                      type = "normal" },
    { pattern = "@%l[%w_]*",                          type = "string" },
  },
symbols = {   
    ["false"] = patType("boolean", "literal"),
    ["true"] = patType("boolean", "literal"),
    
    ["int"] = patType("type", "keyword2"),
    ["float"] = patType("type", "keyword2"),
    ["char"] = patType("type", "keyword2"),
    ["string"] = patType("type", "keyword2"),
    ["bool"] = patType("type", "keyword2"),
    ["list"] = patType("type", "keyword2"),
    ["array"] = patType("type", "keyword2"),
    ["exn"] = patType("type", "keyword2"),   
    ["option"] = patType("type", "keyword2"),
    ["unit"] = patType("type", "keyword2"),
    ["ref"] = patType("type", "keyword2"),
    ["Some"] = patType("type", "keyword2"),
    ["None"] = patType("type", "keyword2"),
    
    ["and"] = "keyword",
    ["as"] = "keyword",
    ["assert"] = "keyword",
    ["asr"] = "keyword",
    ["begin"] = "keyword",
    ["class"] = "keyword",
    ["constraint"] = "keyword",
    ["do"] = "keyword",
    ["done"] = "keyword",
    ["downto"] = "keyword",
    ["else"] = "keyword",
    ["end"] = "keyword",
    ["exception"] = "keyword",
    ["external"] = "keyword",
    ["for"] = "keyword",
    ["fun"] = "keyword",
    ["function"] = "keyword",
    ["functor"] = "keyword",
    ["if"] = "keyword",
    ["in"] = "keyword",
    ["include"] = "keyword",
    ["inherit"] = "keyword",
    ["initializer"] = "keyword",
    ["land"] = "keyword",
    ["lazy"] = "keyword",
    ["let"] = "keyword",
    --["let*"] = "keyword",
    --["let+"] = "keyword",
    ["lor"] = "keyword",
    ["lsl"] = "keyword",
    ["lsr"] = "keyword",
    ["lxor"] = "keyword",
    ["match"] = "keyword",
    ["method"] = "keyword",
    ["mod"] = "keyword",
    ["module"] = "keyword",
    ["mutable"] = "keyword",
    ["new"] = "keyword",
    ["nonrec"] = "keyword",
    ["object"] = "keyword",
    ["of"] = "keyword",
    ["open"] = "keyword",
    ["or"] = "keyword",
    ["private"] = "keyword",
    ["rec"] = "keyword",    
    ["sig"] = "keyword",
    ["struct"] = "keyword",
    ["then"] = "keyword",
    ["to"] = "keyword",    
    ["try"] = "keyword",
    ["type"] = "keyword",
    ["val"] = "keyword",
    ["virtual"] = "keyword",
    ["when"] = "keyword",
    ["while"] = "keyword",
    ["with"] = "keyword",    
  },
}

