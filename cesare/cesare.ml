let alphabet = "abcdefghijklmnopqrstuvwxyz ?!,;."

(* recupera la posizione (indice) all'interno dell'alfabeto *)
let pos = String.index alphabet

(* recupera il carattere dato l'indice all'interno dell'alfabeto *)
let char = String.get alphabet

(* la lunghezza dell'alfabeto *)
let len = String.length alphabet

(* operazione di modulo *)
let ( % ) a b =
  let m = a mod b in
  if m < 0 then m + b else m
;;

let cesare cn pn al = (cn + pn) % al
let de_cesare cn pn al = (cn - pn) % al

(* applica la funzione per ogni carattere della stringa *)
let crypt s n = String.map (fun c -> cesare (pos c) n len |> char) s

(* applica la funzione per ogni carattere della stringa *)
let decrypt s n = String.map (fun c -> de_cesare (pos c) n len |> char) s

let () =
  let text = "stasera andiamo al cinema?" in
  let text' = crypt (crypt text 3) 12 in
  print_endline text';
  let detext = decrypt text' 15 in
  print_endline detext
;;
