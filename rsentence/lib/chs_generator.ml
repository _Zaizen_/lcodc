(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 * chain_sentence_generator
 *)

open Helpers.File_utils
open Helpers.String_utils

type modeltype =
  | ModelAsFile of string
  | ModelAsString of string

let _rnd_init = Random.self_init ()

let dump ~dump_chain ~chain =
  if dump_chain
  then write_file "rsentence-chain-dump.txt" (Helpers.dump_chain chain)
  else ()
;;

let make_op ~word_case =
  match word_case with
  | 1 -> words ~transform:String.lowercase_ascii
  | 2 -> words ~transform:String.uppercase_ascii
  | _ -> words ~transform:(fun s -> s)
;;

let make_words ~model ~model_compact ~shuffle_model ~op =
  match model with
  | ModelAsFile path ->
    lines_of_file path ~op ~compact:model_compact ~shuffle:shuffle_model
  | ModelAsString text ->
    lines_of_string text ~op ~compact:model_compact ~shuffle:shuffle_model
;;

let make_chain ~prealloc ~chain_size ~words = Mchain.mchain ~prealloc chain_size words

let make_starts ~model_compact ~chain_size ~words ~chain =
  if model_compact
  then Mchain.get_firsts_raw chain
  else Mchain.get_firsts chain_size words
;;

let make_sentences ~starts ~chain ~rec_limit ~view_full ~runs =
  let sentences =
    List.map
      (fun _ ->
        let sentence = Mchain.compose starts chain rec_limit in
        if view_full
        then String.concat " " sentence
        else Helpers.get_only_stopped (String.concat " " sentence))
      (Helpers.List_utils.range (if runs < 1 then 1 else runs))
  in
  String.capitalize_ascii (String.concat " \n" sentences)
;;

let print_verbose ~verbose phase message =
  if verbose then Printf.printf "%s -> %s\n%!" phase message else ()
;;

let generate
  ?(prealloc = 100)
  ?(chain_size = 2)
  ?(rec_limit = 100)
  ?(dump_chain = false)
  ?(word_case = 0)
  ?(view_full = false)
  ?(runs = 1)
  ?(model_compact = false)
  ?(shuffle_model = false)
  ?(verbose = false)
  model
  =
  print_verbose ~verbose "BEGIN: " "Start processing...";
  print_verbose ~verbose "PHASE: " "Select word operation...";
  let op = make_op ~word_case in
  print_verbose ~verbose "PHASE: " "Make words list...";
  let words = make_words ~model ~model_compact ~shuffle_model ~op in
  print_verbose ~verbose "PHASE: " "Make naive Markov chain...";
  let chain = make_chain ~prealloc ~chain_size ~words in
  print_verbose ~verbose "PHASE: " "Dump naive Markov chain...";
  dump ~dump_chain ~chain;
  print_verbose ~verbose "PHASE: " "Make starts...";
  let starts = make_starts ~model_compact ~chain_size ~words ~chain in
  print_verbose ~verbose "PHASE: " "Make sentences...";
  make_sentences ~starts ~chain ~rec_limit ~view_full ~runs
;;
