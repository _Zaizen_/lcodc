open Hashtbl
open Helpers.List_utils
open Helpers.Hashtbl_utils

let get_firsts n words_list =
  List.fold_left (fun acc words -> slice 0 (n - 1) words :: acc) [] words_list
  |> List.sort_uniq compare
;;

let get_firsts_raw chain =
  Hashtbl.to_seq_keys chain |> List.of_seq |> List.sort_uniq compare
;;

let mchain ?(prealloc = 100) n words_list =
  let slices' = slices 0 (n - 1) in
  let rec chain result n ws =
    let _, key, rest = slices' ws in
    match slices' rest with
    | [], [], [] -> result
    | (_, _ :: _, _) as sls ->
      let _, value, _ = sls in
      push result key value;
      chain result n rest
    | _ -> result
  in
  let chain =
    List.fold_left
      (fun acc words -> chain acc n words)
      (Hashtbl.create prealloc)
      words_list
  in
  chain
;;

let mchain_assoc n words_list =
  let slices' = slices 0 (n - 1) in
  let rec chain result n ws =
    let _, key, rest = slices' ws in
    match slices' rest with
    | [], [], [] -> result
    | (_, _ :: _, _) as sls ->
      let _, value, _ = sls in
      let result' = push_assoc result key value in
      chain result' n rest
    | _ -> result
  in
  List.fold_left (fun acc words -> chain acc n words) [] words_list
;;

let compose starts chain lmt =
  let start = rnd_element starts in
  let rec scan result k limit =
    match find_opt chain k with
    | None -> result
    | Some v ->
      let v' = find_rnd_value v "" in
      (match last v' = ".", limit > lmt with
       | true, _ -> result @> v'
       | _, true -> result @> v' @> [ "[---]" ]
       | false, false -> scan (result @> v') v' (limit + 1))
  in
  scan start start 0
;;
