module EmittedTypes = struct
  type t =
    | Literal of string
    | Strong of t list
    | Emphasis of string
    | Underline of string
    | Heading1 of string
    | Heading2 of string
    | HyperLink of t list
    | HyperLinkImage of t list
    | Paragraph of t list
    | HyperLinkTitle of string
    | HyperLinkUrl of string
    | SoftBreak of string
    | List of t list
    | ListElement of string
end;;

module RuleTypes = struct
  type t =
    | Literal
    | Strong
    | Emphasis
    | Underline
    | Heading1
    | Heading2
    | HyperLink
    | HyperLinkImage
    | Paragraph
    | HyperLinkTitle
    | HyperLinkUrl
    | SoftBreak
    | List
    | ListElement
end;;

module PhaseTypes = struct
  type t =
    | Literal of char list
    | Delimited of char list
end;;

type rule_state =
  | FailRule of char list * char list
  | SuccessRule of char list * char list;;

type delimiters =
  | Delim of char list
  | EmptyDelim;;

type rule =
  { start : delimiters
  ; stop : delimiters
  ; t : RuleTypes.t
  ; emit : char list -> EmittedTypes.t
  };;

let delim_or_empty delim =
  match delim with
  | Delim delim -> delim
  | EmptyDelim -> []
;;

let read_text_ch ic =
  let rec read content ic =
    try
      let content' = input_line ic :: content in
      read content' ic
    with
    | End_of_file -> Some (content |> List.rev |> String.concat "\n")
    | _ -> None
  in
  read [] ic
;;

let read_file path =
  let in_c = open_in path in
  let content = read_text_ch in_c in
  match content with
  | Some s -> s
  | None -> ""
;;

let write_text_to_file text filename =
  let out_file = open_out filename in
  Printf.fprintf out_file "%s" text;
  close_out out_file
;;

let explode str = List.of_seq (String.to_seq str);;
let implode chars = String.of_seq (List.to_seq chars);;

let starts_with prefix chars =
  let rec scan_delim = function
    | [], chars -> Some chars
    | p :: prefix, c :: chars when p = c -> scan_delim (prefix, chars)
    | _ -> None
  in
  match prefix with
  | EmptyDelim -> None
  | Delim prefix -> scan_delim (prefix, chars)
;;

let delimited rule chars =
  let { start; stop; _ } = rule in
  let rec get_body acc chars =
    match starts_with stop chars, chars with
    | Some chars, _ -> Some (SuccessRule (List.rev acc, chars))
    | _, c :: rest -> get_body (c :: acc) rest
    | _, _ ->
      let prefix = delim_or_empty start in
      Some (FailRule (prefix, List.rev acc))
  in
  match starts_with start chars with
  | Some chars -> get_body [] chars
  | _ -> None
;;

let status (rules, defrule) chars =
  let is opt =
    match opt with
    | None -> false
    | Some _ -> true
  in
  let rule =
    List.find_opt
      (fun rule -> rule.start <> EmptyDelim && is (starts_with rule.start chars))
      rules
  in
  match rule with
  | None -> PhaseTypes.Literal chars, defrule
  | Some rule -> PhaseTypes.Delimited chars, rule
;;

let scanner chars (rules, defrule) =
  let rec parser span acc chars =
    let apply_rule rule span =
      let emit rule delim_value =
        match delim_value with
        | delim, (_ :: _ as chars) ->
          let span = rule.emit delim :: span in
          parser span [] chars
        | delim, [] -> rule.emit delim :: span
      in
      match delimited rule chars with
      | None -> parser span acc chars
      | Some (SuccessRule (delim, value)) -> emit rule (delim, value)
      | Some (FailRule (delim, value)) -> emit defrule (delim, value)
    in
    let apply_defrule ?c () =
      match c with
      | Some c -> defrule.emit (List.rev (c :: acc)) :: span
      | _ -> if acc = [] then span else defrule.emit (List.rev acc) :: span
    in
    match status (rules, defrule) chars with
    | PhaseTypes.Literal (c :: rest), _rule ->
      (match rest with
      | [] -> apply_defrule ~c ()
      | _ -> parser span (c :: acc) rest)
    | _, rule -> apply_defrule () |> apply_rule rule
  in
  parser [] [] chars |> List.rev
;;

let parse text rules = scanner (explode text) rules;;

let def_rule () =
  { t = RuleTypes.Literal
  ; start = EmptyDelim
  ; stop = EmptyDelim
  ; emit = (fun chars -> EmittedTypes.Literal (implode chars))
  }
;;

let stext_hyperlink_subrules () =
  [ { t = RuleTypes.HyperLinkTitle
    ; start = Delim [ '[' ]
    ; stop = Delim [ ']' ]
    ; emit = (fun chars -> EmittedTypes.HyperLinkTitle (implode chars))
    }
  ; { t = RuleTypes.HyperLinkUrl
    ; start = Delim [ '(' ]
    ; stop = Delim [ ')' ]
    ; emit = (fun chars -> EmittedTypes.HyperLinkUrl (implode chars))
    }
  ]
;;

let stext_list_subrules () =
  [ { t = RuleTypes.ListElement
    ; start = Delim [ '+'; ' ' ]
    ; stop = Delim [ '\n' ]
    ; emit = (fun chars -> EmittedTypes.ListElement (implode chars))
    }
  ]
;;

let rec stext_rules () =
  [ { t = RuleTypes.Paragraph
    ; start = Delim [ '$'; ' ' ]
    ; stop = Delim [ '\n'; '\n' ]
    ; emit =
        (fun chars ->
          let rules =
            List.filter (fun r -> r.t <> RuleTypes.Paragraph) (stext_rules ())
          in
          let para = scanner chars (rules, def_rule ()) in
          EmittedTypes.Paragraph para)
    }
  ; { t = RuleTypes.List
    ; start = Delim [ '('; '+'; ' ' ]
    ; stop = Delim [ ')' ]
    ; emit =
        (fun chars ->
          let chars = [ '+'; ' ' ] @ chars @ [ '\n' ] in
          let para = scanner chars (stext_list_subrules (), def_rule ()) in
          EmittedTypes.List para)
    }
  ; { t = RuleTypes.Heading1
    ; start = Delim [ '#'; ' ' ]
    ; stop = Delim [ '\n'; '\n' ]
    ; emit = (fun chars -> EmittedTypes.Heading1 (implode chars))
    }
  ; { t = RuleTypes.Heading2
    ; start = Delim [ '#'; '#'; ' ' ]
    ; stop = Delim [ '\n'; '\n' ]
    ; emit = (fun chars -> EmittedTypes.Heading2 (implode chars))
    }
  ; { t = RuleTypes.Strong
    ; start = Delim [ '*' ]
    ; stop = Delim [ '*' ]
    ; emit =
        (fun chars ->
          let rules = List.filter (fun r -> r.t <> RuleTypes.Strong) (stext_rules ()) in
          let parts = scanner chars (rules, def_rule ()) in
          EmittedTypes.Strong parts)
    }
  ; { t = RuleTypes.Emphasis
    ; start = Delim [ '/' ]
    ; stop = Delim [ '/' ]
    ; emit = (fun chars -> EmittedTypes.Emphasis (implode chars))
    }
  ; { t = RuleTypes.Underline
    ; start = Delim [ '_' ]
    ; stop = Delim [ '_' ]
    ; emit = (fun chars -> EmittedTypes.Underline (implode chars))
    }
  ; { t = RuleTypes.HyperLink
    ; start = Delim [ '[' ]
    ; stop = Delim [ ')' ]
    ; emit =
        (fun chars ->
          let chars = [ '[' ] @ chars @ [ ')' ] in
          let hyper = scanner chars (stext_hyperlink_subrules (), def_rule ()) in
          EmittedTypes.HyperLink hyper)
    }
  ; { t = RuleTypes.HyperLinkImage
    ; start = Delim [ '!'; '[' ]
    ; stop = Delim [ ')' ]
    ; emit =
        (fun chars ->
          let chars = [ '[' ] @ chars @ [ ')' ] in
          let hyper = scanner chars (stext_hyperlink_subrules (), def_rule ()) in
          EmittedTypes.HyperLinkImage hyper)
    }
  ; { t = RuleTypes.SoftBreak
    ; start = Delim [ '\\' ]
    ; stop = Delim [ '\\' ]
    ; emit = (fun chars -> EmittedTypes.SoftBreak (implode chars))
    }
  ]
;;

let literal s = Printf.sprintf "%s" s;;
let heading1 s = Printf.sprintf "<h1>%s</h1>" s;;
let heading2 s = Printf.sprintf "<h2>%s</h2>" s;;

(*let heading3 s = Printf.sprintf "<h3>%s</h3>" s
let heading4 s = Printf.sprintf "<h4>%s</h4>" s
let heading5 s = Printf.sprintf "<h5>%s</h5>" s
let heading5 s = Printf.sprintf "<h6>%s</h6>" s
*)
let emphasis s = Printf.sprintf "<em>%s</em>" s;;
let underline s = Printf.sprintf "<u>%s</u>" s;;
let softbreak s = if s = "" then "<br />" else Printf.sprintf "<br />%s<br />" s;;
let list_element s = Printf.sprintf "<li>%s</li>" s;;

let hyperLink tree =
  match tree with
  | EmittedTypes.HyperLinkTitle t :: EmittedTypes.HyperLinkUrl u :: _ ->
    Printf.sprintf {|<a href="%s" title="%s">%s</a>|} u t t
  | _ -> ""
;;

let hyperLink_image tree =
  match tree with
  | EmittedTypes.HyperLinkTitle t :: EmittedTypes.HyperLinkUrl u :: _ ->
    Printf.sprintf {|<img src="%s" title="%s" alt="%s" />|} u t t
  | _ -> "aaa"
;;

let rec strong tree =
  let match_emits acc e =
    let acc, _ = match_span_emits (acc, e) in
    acc
  in
  let elements = List.fold_left match_emits [] tree |> List.rev in
  Printf.sprintf "<strong>%s</strong>" (String.concat "" elements)

and paragraph tree =
  let match_emits acc e =
    let acc, _ = match_span_emits (acc, e) |> match_block_emits in
    acc
  in
  let elements = List.fold_left match_emits [] tree |> List.rev in
  Printf.sprintf "<p>%s</p>" (String.concat "" elements)

and list tree =
  let match_emits acc e =
    let acc, _ = match_span_emits (acc, e) |> match_block_emits in
    acc
  in
  let elements = List.fold_left match_emits [] tree |> List.rev in
  Printf.sprintf "<ul>%s</ul>" (String.concat "" elements)

and html tree =
  let match_emits acc e =
    let acc, _ = match_span_emits (acc, e) |> match_block_emits in
    acc
  in
  let elements = List.fold_left match_emits [] tree |> List.rev in
  Printf.sprintf {|<html><head></head><body>%s</body></html>|} (String.concat "" elements)

and match_block_emits (acc, e) =
  match e with
  | EmittedTypes.Strong elements -> strong elements :: acc, e
  | EmittedTypes.Paragraph tree -> paragraph tree :: acc, e
  | EmittedTypes.List tree -> list tree :: acc, e
  | _ -> acc, e

and match_span_emits (acc, e) =
  match e with
  | EmittedTypes.Literal s -> literal s :: acc, e
  | EmittedTypes.Heading1 s -> heading1 s :: acc, e
  | EmittedTypes.Heading2 s -> heading2 s :: acc, e
  | EmittedTypes.Emphasis s -> emphasis s :: acc, e
  | EmittedTypes.Underline s -> underline s :: acc, e
  | EmittedTypes.HyperLink link -> hyperLink link :: acc, e
  | EmittedTypes.HyperLinkImage link -> hyperLink_image link :: acc, e
  | EmittedTypes.SoftBreak s -> softbreak s :: acc, e
  | EmittedTypes.ListElement s -> list_element s :: acc, e
  | _ -> acc, e
;;
