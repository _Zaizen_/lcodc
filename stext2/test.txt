# C'era una volta...

$ C'era una volta... – Un re! – *diranno* subito i miei piccoli * lettori.

$ No, ragazzi, /avete sbagliato/. C'era una volta un _pezzo di legno_.

$ Non era un *legno di /lusso/*, ma un semplice /pezzo da catasta/, \\ 
di quelli che d'inverno si mettono nelle *stufe* e nei *caminetti* per \\
accendere il fuoco e per riscaldare le stanze. 

\Le avventure di pinocchio\

[Le avventure di pinocchio](http:://aaaa.com) 

![Pinocchio](http://getdrawings.com/cliparts/clipart-models-32.jpg) 

## Lista dentro paragrafo

$  Elenco di linee 
(+ linea 1
 + linea 2
 + linea 3
 + linea 4
)



